package mc.Mitchellbrine.virusMod.network;


import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

/**
 * Created by Mitchellbrine on 2014.
 */
public class PacketHandler {

    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel("VirusMod");

    public static void init()
    {
        INSTANCE.registerMessage(HeatPacket.class, HeatPacket.class, 0, Side.CLIENT);
    }

}
