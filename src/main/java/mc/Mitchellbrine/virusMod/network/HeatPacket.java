package mc.Mitchellbrine.virusMod.network;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class HeatPacket
        implements IMessage, IMessageHandler<HeatPacket,IMessage>{

    private float newHeat;

    public HeatPacket() {}

    public HeatPacket(float heat) {
        this.newHeat = heat;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        newHeat = buf.readFloat();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeFloat(newHeat);
    }

    @Override
    public IMessage onMessage(HeatPacket message, MessageContext ctx) {

        Minecraft.getMinecraft().thePlayer.getEntityData().setFloat("heatDC",message.getHeat());
        return null;
    }

    public float getHeat() {
        return this.newHeat;
    }

    public void setHeat(float heat) {
        this.newHeat = heat;
    }



}
