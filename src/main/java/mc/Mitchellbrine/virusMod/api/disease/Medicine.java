package mc.Mitchellbrine.virusMod.api.disease;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

import java.util.List;

public class Medicine extends Item {
    private Potion medicine;

    public Medicine(Potion medicineEffect) {
        medicine = medicineEffect;
        this.setCreativeTab(CreativeTabs.tabBrewing);
        //this.setTextureName("virusMod:medicine");
    }

    @Override
    public int getMaxItemUseDuration(ItemStack itemstack) {
        return 32;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack itemstack) {
        return EnumAction.EAT;
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack itemstack, World world, EntityPlayer player) {
        if (player.isPotionActive(medicine)) {
            player.addPotionEffect(new PotionEffect(medicine.id, player.getActivePotionEffect(medicine).getDuration() + 3000, 0, true,false));
        }
        else {
            player.addPotionEffect(new PotionEffect(medicine.id, 3000, 0, true,false));
        }
        super.onItemUseFinish(itemstack, world, player);
        return itemstack;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void addInformation(ItemStack itemstack, EntityPlayer player, List list, boolean par4) {
        list.add("Medicine Type: " + this.getUnlocalizedName().substring(5));
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }

}
