package mc.Mitchellbrine.virusMod.api.player;

import net.minecraft.entity.player.EntityPlayer;

public class LifetimeDiseases {

    public static boolean getParkinsons(EntityPlayer player) {

    if (player.getEntityData().hasKey("gottenParkinsons")) {
        return player.getEntityData().getBoolean("gottenParkinsons");
    }
        else {
        return false;
    }

    }

    public static boolean getAstigmatism(EntityPlayer player) {

    if (player.getEntityData().hasKey("gottenAstigmatism")) {
        return player.getEntityData().getBoolean("gottenAstigmatism");
    }
        else {
        return false;
    }

    }

    public static boolean setParkinsons(EntityPlayer player, boolean hasGotten) {

      try {
        player.getEntityData().setBoolean("gottenParkinsons",hasGotten);
          return true;
      }
      catch(Exception e) {
          e.printStackTrace();
          return false;
      }

    }

    public static boolean setAstigmatism(EntityPlayer player, boolean hasGotten) {

        try {
            player.getEntityData().setBoolean("gottenAstigmatism",hasGotten);
            return true;
        }
        catch(Exception e) {
            e.printStackTrace();
            return false;
        }

    }


}
