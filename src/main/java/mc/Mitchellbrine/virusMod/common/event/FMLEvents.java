package mc.Mitchellbrine.virusMod.common.event;

import java.util.Random;

import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.init.Items;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.entity.player.PlayerUseItemEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class FMLEvents {
	
	@SubscribeEvent
	public void rabiesAndPotionSaving(PlayerUseItemEvent.Start event) {
                for (int i = 0; i < VirusMod.milkProofDiseases.size();i++) {
                    if (DiseaseHandler.isDiseaseActive(event.entityPlayer,VirusMod.milkProofDiseases.get(i))) {
                        if (event.item.getItem() == Items.milk_bucket) {
                            event.setCanceled(true);
                        }
                    }
                }

		if (event.item != null && VirusMod.pPlagueFoods.contains(event.item.getItem())) {
			Random random = new Random();
			
			int plagueChance = random.nextInt(100);
			
			if (plagueChance <= ((Disease)VirusMod.pPlague).getChance()) {
                if (!event.entityPlayer.getEntityData().hasKey("vaccinePneumonicPlague") || !event.entityPlayer.getEntityData().getBoolean("vaccinePneumonicPlague")) {
                    event.entityPlayer.addPotionEffect(new PotionEffect(VirusMod.pPlague.id, 1200, 0, true,false));
                }
			}
			
		}
		
		
	}

    @SubscribeEvent
    public void waterSavingTheDay(PlayerUseItemEvent.Finish event) {
        if (event.item != null && event.item.getItem() == Items.potionitem && event.item.getItemDamage() == 0) {
            if (DiseaseHandler.isDiseaseActive(event.entityPlayer,VirusMod.heatStroke)) {
                DiseaseHandler.removeDisease(event.entityPlayer, VirusMod.heatStroke.id);
            }
            if (!event.entityPlayer.getEntityData().hasKey("heatDC")) {
                event.entityPlayer.getEntityData().setFloat("heatDC", 37.0F);
            } else {
                float heat = event.entityPlayer.getEntityData().getFloat("heatDC");
                event.entityPlayer.getEntityData().setFloat("heatDC",heat - 0.5F);
            }
        }
    }
}
