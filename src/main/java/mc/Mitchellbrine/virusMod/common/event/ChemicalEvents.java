package mc.Mitchellbrine.virusMod.common.event;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.entity.EntityRat;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DamageSource;
import net.minecraftforge.event.entity.player.EntityInteractEvent;

import java.util.Random;

public class ChemicalEvents {

    @SubscribeEvent
    public void interact(EntityInteractEvent event) {
        Random random = new Random();

        if (event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer.getCurrentEquippedItem().getItem() == VirusMod.chemicalExtractor && event.entityPlayer.getCurrentEquippedItem().getItemDamage() == 0) {
            if (event.target instanceof EntityRat) {
                if (random.nextInt(100) <= 5) {
                    event.target.attackEntityFrom(DamageSource.causeMobDamage(event.entityPlayer), 1.0F);
                    event.entityPlayer.setCurrentItemOrArmor(0, new ItemStack(VirusMod.chemicalExtractor, 1, VirusMod.bubonicPlague.id));
                }
            }
            if (event.target instanceof EntityWolf) {
                if (!((EntityWolf) event.target).isTamed()) {
                    if (random.nextInt(100) <= 17) {
                        event.target.attackEntityFrom(DamageSource.causeMobDamage(event.entityPlayer), 1.0F);
                        event.entityPlayer.setCurrentItemOrArmor(0, new ItemStack(VirusMod.chemicalExtractor, 1, VirusMod.rabies.id));
                    }
                }
            }
            if (event.target instanceof EntityPlayer || event.target instanceof EntityZombie) {
                    if (random.nextInt(100) <= 20) {
                        event.target.attackEntityFrom(DamageSource.causeMobDamage(event.entityPlayer), 1.0F);
                        event.entityPlayer.setCurrentItemOrArmor(0, new ItemStack(VirusMod.chemicalExtractor, 1, VirusMod.influenza.id));
                    }
            }
        }

    }

}
