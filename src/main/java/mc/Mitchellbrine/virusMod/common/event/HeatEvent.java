package mc.Mitchellbrine.virusMod.common.event;

import net.minecraft.util.BlockPos;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import mc.Mitchellbrine.virusMod.network.HeatPacket;
import mc.Mitchellbrine.virusMod.network.PacketHandler;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.DamageSource;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.entity.living.LivingEvent;

import java.util.Random;

@SuppressWarnings("static-access")
public class HeatEvent {

    private static float biomeColdModifier;
    private static float biomeHeatModifier;

    private static float blockColdModifier;
    private static float blockFreezingModifier;
    private static float blockFrozenModifier;

    private static float blockHeatModifier;
    private static float blockScoldingModifier;

    public static boolean blockModifierEnabled;
    public static boolean biomeModifierEnabled;

    public static DamageSource cold = new DamageSource("cold").setDifficultyScaled().setDamageIsAbsolute();
    public static DamageSource heat = new DamageSource("heat").setDifficultyScaled().setDamageIsAbsolute();

    public static void setConfigs(Configuration config) {

        config.load();

        biomeColdModifier = (float)config.get("biome","Cold Biome Modifier",0.04,"Sets the amount of thermal energy decreased from being in cold biomes").getDouble(0.04);
        biomeHeatModifier = (float)config.get("biome","Hot Biome Modifier",0.04,"Sets the amount of thermal energy increased from being in hot biomes").getDouble(0.04);
        blockColdModifier = (float)config.get("block","Cold Block Modifier",0.01,"Sets the amount of thermal energy decreased from being near cold blocks").getDouble(0.01);
        blockFreezingModifier = (float)config.get("block","Freezing Block Modifier",0.02,"Sets the amount of thermal energy decreased from being near freezing blocks").getDouble(0.02);
        blockFrozenModifier = (float)config.get("block","Frozen Block Modifier",0.03,"Sets the amount of thermal energy decreased from being near frozen blocks").getDouble(0.03);
        blockHeatModifier = (float)config.get("block","Hot Block Modifier",0.01,"Sets the amount of thermal energy increased from being near hot blocks").getDouble(0.01);
        blockScoldingModifier = (float)config.get("block","Very Hot Block Modifier",0.03,"Sets the amount of thermal energy increased from being near very hot blocks").getDouble(0.03);

        blockModifierEnabled = config.get("settings","Check for heat from blocks?",true,"Sets if we check for blocks in heat modifiers").getBoolean(true);
        biomeModifierEnabled = config.get("settings","Check for heat from biomes?",true,"Sets if we check for biomes in heat modifiers").getBoolean(true);

        config.save();

    }

    @SubscribeEvent
    public void livingUpdate(LivingEvent.LivingUpdateEvent event) {
        if (event.entityLiving instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.entityLiving;

            int x = (int) Math.floor(player.posX);
            int y = (int) Math.floor(player.posY);
            int z = (int) Math.floor(player.posZ);

                if (!player.getEntityData().hasKey("heatDC")) {
                    player.getEntityData().setFloat("heatDC", 37.0F);
                } else {
                    float heat = player.getEntityData().getFloat("heatDC");
                    if (player.worldObj.getWorldTime() % 20 == 0) {

                            if (player.worldObj.provider.getDimensionId() == 0) {
                                if (biomeModifierEnabled || blockModifierEnabled) {
                                    for (int xx = x - 1; xx < x + 2; xx++) {
                                        for (int yy = y - 1; yy < y + 2; yy++) {
                                            for (int zz = z - 1; zz < z + 2; zz++) {

                                                if (biomeModifierEnabled) {
                                                    if (player.worldObj.getBiomeGenForCoords(new BlockPos(xx, yy, zz)).temperature <= 0.25F) {
                                                        player.getEntityData().setFloat("heatDC", heat - biomeColdModifier);
                                                    }
                                                    if (player.worldObj.getBiomeGenForCoords(new BlockPos(xx, yy, zz)).temperature > 0.9F) {
                                                        player.getEntityData().setFloat("heatDC", heat + biomeHeatModifier);
                                                    }
                                                }


                                                if (blockModifierEnabled) {
                                                /*if (player.worldObj.getBlockLightValue(xx, yy, zz) > 0.5F && player.worldObj.getBlockLightValue(xx, yy, zz) < 0.75F) {
                                                    player.getEntityData().setFloat("heatDC", heat + blockHeatModifier);
                                                }
                                                if (player.worldObj.getBlockLightValue(xx, yy, zz) >= 0.75F) {
                                                    player.getEntityData().setFloat("heatDC", heat + blockScoldingModifier);
                                                } */

                                                    if (player.worldObj.getBlockState(new BlockPos(new BlockPos(xx, yy, zz))).getBlock() == Blocks.fire || player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.lava || player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.flowing_lava) {
                                                        player.getEntityData().setFloat("heatDC", heat + blockHeatModifier);
                                                    }

                                                    if (player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.glowstone || player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.lit_redstone_lamp) {
                                                        player.getEntityData().setFloat("heatDC", heat + blockScoldingModifier);
                                                    }

                                                    if (player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.flowing_water || player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.water) {
                                                        player.getEntityData().setFloat("heatDC", heat - blockColdModifier);
                                                    }
                                                    if (player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.snow || player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.snow_layer) {
                                                        player.getEntityData().setFloat("heatDC", heat - blockFreezingModifier);
                                                    }
                                                    if (player.worldObj.getBlockState(new BlockPos(xx, yy, zz)).getBlock() == Blocks.ice) {
                                                        player.getEntityData().setFloat("heatDC", heat - blockFrozenModifier);
                                                    }
                                                }
                                            }
                                        }
                                    }
                            }
                            }

                            float newHeat = player.getEntityData().getFloat("heatDC");

                            if (player instanceof EntityPlayerMP) {
                                PacketHandler.INSTANCE.sendTo(new HeatPacket(newHeat), (EntityPlayerMP)player);
                            }

                            Random random = new Random(((EntityPlayer) event.entityLiving).worldObj.getWorldTime());

                            if (!player.worldObj.isRemote) {
                                if (newHeat < 35.0F && newHeat > 25.0F) {
                                    if (VirusMod.useDebugDiseaseMessages == 1) {
                                        VirusMod.debugLogger.info("Player: " + player.getName() + " | Temperature State: Hypothermia | " + newHeat);
                                    }
                                    if (!event.entityLiving.getEntityData().hasKey("vaccineInfluenza") || !event.entityLiving.getEntityData().getBoolean("vaccineInfluenza")) {
                                        if (!event.entityLiving.isPotionActive(VirusMod.influenza) && !DiseaseHandler.isDiseaseActive(player,VirusMod.influenza)) {
                                            if (random.nextInt(((Disease)VirusMod.influenza).getChance()) == 0) {
                                                event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.influenza.id, 1200, 0, true,false));
                                            }
                                        }
                                    }
                                } else if (newHeat <= 37.5F && newHeat > 35.0) {
                                    if (VirusMod.useDebugDiseaseMessages == 1) {
                                        VirusMod.debugLogger.info("Player: " + player.getName() + " | Temperature State: Normal | " + newHeat);
                                    }
                                        if (DiseaseHandler.isDiseaseActive(player,VirusMod.influenza)) {
                                                DiseaseHandler.removeDisease(event.entityLiving,VirusMod.influenza.id);
                                        }
                                        if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.heatStroke.id)) {
                                            DiseaseHandler.removeDisease(event.entityLiving,VirusMod.heatStroke.id);
                                        }

                                    } else if (newHeat > 37.5F && newHeat <= 38.3F) {
                                    if (VirusMod.useDebugDiseaseMessages == 1) {
                                        VirusMod.debugLogger.info("Player: " + player.getName() + " | Temperature State: Fever | " + newHeat);
                                    }
                                    // Fever State (Fever)
                                } else if (newHeat > 40.0 && newHeat < 45.0) {
                                    if (VirusMod.useDebugDiseaseMessages == 1) {
                                        VirusMod.debugLogger.info("Player: " + player.getName() + " | Temperature State: Hyperpyrexia | " + newHeat);
                                    }
                                    if (random.nextInt(((Disease)VirusMod.heatStroke).getChance()) == 0) {
                                        event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.heatStroke.id, 1200, 0, true,false));
                                    }
                                    // Hyperpyrexia State (Heat Stroke)
                                } else if (newHeat <= 25.0F) {
                                    player.attackEntityFrom(this.cold,10000F);
                                } else if (newHeat >= 45.0F) {
                                    player.attackEntityFrom(this.heat,10000F);
                                }
                            }

                    }
            }

        }
    }

}
