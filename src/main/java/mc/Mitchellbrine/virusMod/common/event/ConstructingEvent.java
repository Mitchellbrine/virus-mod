package mc.Mitchellbrine.virusMod.common.event;

import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import mc.Mitchellbrine.virusMod.common.entity.player.ParkinsonsYOGO;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.EntityEvent;

public class ConstructingEvent {

    @SubscribeEvent
    public void onEntityConstructing(EntityEvent.EntityConstructing event)
    {

        if (event.entity instanceof EntityPlayer && ParkinsonsYOGO.get((EntityPlayer) event.entity) == null)
            ParkinsonsYOGO.register((EntityPlayer) event.entity);

        if (event.entity instanceof EntityPlayer && event.entity.getExtendedProperties(ParkinsonsYOGO.EXT_PROP_NAME) == null)
            event.entity.registerExtendedProperties(ParkinsonsYOGO.EXT_PROP_NAME, new ParkinsonsYOGO((EntityPlayer) event.entity));
    }
}
