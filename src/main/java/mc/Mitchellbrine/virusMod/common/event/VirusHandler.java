package mc.Mitchellbrine.virusMod.common.event;

import java.util.Iterator;
import java.util.Random;

import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.entity.doctor.EntityDoctor;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import mc.Mitchellbrine.virusMod.common.potion.Effects;
import mc.Mitchellbrine.virusMod.util.ChatUtils;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.monster.EntityZombie;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.entity.passive.EntityWolf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.stats.StatList;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.DamageSource;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.event.entity.item.ItemEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.entity.player.PlayerDropsEvent;

import javax.swing.text.html.parser.Entity;

public class VirusHandler {

    public static DamageSource brainReanimation = new DamageSource("zombie").setDamageBypassesArmor().setDamageIsAbsolute();

	@SubscribeEvent
	public void onLivingUpdate(LivingUpdateEvent event) {
			Random random = new Random();	

			// Bubonic Plague

            if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.heatStroke)) {
                Effects.Symptoms.execute(event.entityLiving,VirusMod.heatStroke);
            }
			
			if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.bubonicPlague)) {
					Effects.Symptoms.execute(event.entityLiving,VirusMod.bubonicPlague);

                if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.bubonicPlague.id) == 1) {
                    Effects.Death.execute(random, event.entityLiving, 75);
                }
			}
			
			// Rabies
			
			if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.rabies)) {

                if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.rabies.id) <= 9600 && event.entityLiving.getEntityData().getInteger("disease"+VirusMod.rabies.id) > 0) {

                    Effects.Hydrophobia.execute(event.entityLiving);

                    int nauseaChance = random.nextInt(100);
                    int darknessChance = random.nextInt(1000);

                    if (!event.entityLiving.isPotionActive(VirusMod.level1Medicine)) {

                        Effects.Symptoms.execute(event.entityLiving, VirusMod.rabies);

                        if (darknessChance == 0) {
                            event.entityLiving.addPotionEffect(new PotionEffect(Potion.blindness.id, 20, 0, true,false));
                            if (event.entityLiving.getHealth() > 6.0F) {
                                event.entityLiving.attackEntityFrom(DamageSource.generic, 1.0F);
                            }
                        }
                    }

                    if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.rabies.id) == 20) {

                        Effects.Death.execute(random, event.entityLiving, 50);
                    }

                }
					
		    }

        if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.brainReanimate)) {
            Effects.Symptoms.execute(event.entityLiving,VirusMod.brainReanimate);
            if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.brainReanimate.id) == 1) {
                if (event.entityLiving instanceof EntityPlayer) {
                    EntityPlayer player = (EntityPlayer) event.entityLiving;
                    EntityZombie zombie = new EntityZombie(event.entityLiving.worldObj);
                    zombie.setCustomNameTag(event.entityLiving.getName());
                    zombie.setCurrentItemOrArmor(0,player.getCurrentEquippedItem());
                    zombie.setCurrentItemOrArmor(1,player.getCurrentArmor(0));
                    zombie.setCurrentItemOrArmor(2,player.getCurrentArmor(1));
                    zombie.setCurrentItemOrArmor(3,player.getCurrentArmor(2));
                    zombie.setCurrentItemOrArmor(4,player.getCurrentArmor(3));
                    for (int i = 0; i < 5;i++) {
                        //if (((EntityPlayer) event.entityLiving).worldObj.getGameRules().getGameRuleBooleanValue("keepInventory")) {
                            zombie.setEquipmentDropChance(i, 0.0F);
                        //} else {
                            //zombie.setEquipmentDropChance(i, 1.0F);
                        //}
                    }
                    zombie.setLocationAndAngles(player.posX,player.posY,player.posZ,player.cameraPitch,player.cameraYaw);
                    player.worldObj.spawnEntityInWorld(zombie);
                }
                    event.entityLiving.attackEntityFrom(brainReanimation, 10000.0F);

            }
        }
			
	}
	
	@SubscribeEvent
	public void interact(ItemEvent event) {
        if (DiseaseHandler.isDiseaseActive(event.entity,VirusMod.rabies)) {
            if (event.entityItem.getEntityItem().getItem() == Items.water_bucket || event.entityItem.getEntityItem().getItem() == Items.potionitem) {
                event.entity.attackEntityFrom(DamageSource.drown, 1.0F);
            }
        }
	}
	
	@SubscribeEvent
	public void rabies(LivingAttackEvent event) {
		
		Random random = new Random();
		
		if (event.source.getEntity() instanceof EntityWolf) {
			
			int rabiesChance = random.nextInt(100);
			if (!event.entityLiving.isPotionActive(VirusMod.level1Medicine)) {
			if (rabiesChance >= ((Disease)VirusMod.rabies).getChance()) {
                if (!event.entityLiving.getEntityData().hasKey("vaccineRabies") || !event.entityLiving.getEntityData().getBoolean("vaccineRabies")) {
                    event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.rabies.id, 2400, 0));
                }
			}
			}
		}
		
		if (event.source.getEntity() instanceof EntityZombie || event.source.getEntity() instanceof EntityPlayer) {
			
			int zombieChance = random.nextInt(100);
			
			if (event.source.getEntity() instanceof EntityZombie) {
			if (zombieChance >= ((Disease)VirusMod.brainReanimate).getChance()) {
                event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.brainReanimate.id,3600,0,true,false));
			}
			}
			else if (event.source.getEntity() instanceof EntityPlayer) {
				if (((EntityPlayer)event.source.getEntity()).getCurrentEquippedItem() == null) {
				if (zombieChance >= ((Disease)VirusMod.influenza).getChance()) {
                    if (!event.entityLiving.getEntityData().hasKey("vaccineInfluenza") || !event.entityLiving.getEntityData().getBoolean("vaccineInfluenza")) {
                        event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.influenza.id, 1200, 0, true,false));
                    }
				}
				}
			}
			
			
		}

        if (event.source.getEntity() instanceof EntityPlayer) {

            EntityPlayer player  = (EntityPlayer) event.source.getEntity();

            for (int i = 0; i < Potion.potionTypes.length;i++) {

                if (player.isPotionActive(i)) {
                    if (event.entityLiving instanceof EntityPlayer) {
                            event.entityLiving.addPotionEffect(new PotionEffect(i, player.getActivePotionEffect(Potion.potionTypes[i]).getDuration(), player.getActivePotionEffect(Potion.potionTypes[i]).getAmplifier(), true,false));
                    }
                    else {
                        event.entityLiving.addPotionEffect(new PotionEffect(i, player.getActivePotionEffect(Potion.potionTypes[i]).getDuration(), player.getActivePotionEffect(Potion.potionTypes[i]).getAmplifier(), false,true));
                    }
                }

            }
        }
		
	}

    @SubscribeEvent
    public void livingDeathEvent(LivingDeathEvent event) {
        if (event.entityLiving instanceof EntityDoctor) {
            EntityDoctor doctor = (EntityDoctor) event.entityLiving;
            if (event.source.getEntity() instanceof EntityPlayer) {
                if (doctor.getProfession() != 3) {
                    ChatUtils.broadcastMessage(((EntityDoctor) event.entityLiving).worldObj, new ChatComponentText(doctor.getDoctorName() + " was slain by " + ((EntityPlayer) event.source.getEntity()).getDisplayName()));
                }
            }
        }
    }

    /*@SubscribeEvent
    public void playerDropsEvent(PlayerDropsEvent event){
        if (event.source == brainReanimation) {
            for (EntityItem item : event.drops) {
                for (int i = 0; i < 4; i++) {
                    if (event.entityPlayer.inventory.armorItemInSlot(i) != null && event.entityPlayer.inventory.armorItemInSlot(i).getItem() == item.getEntityItem().getItem() && event.entityPlayer.inventory.armorItemInSlot(i).getItemDamage() == item.getEntityItem().getItemDamage()) {
                        event.drops.remove(item);
                    }
                    if (event.entityPlayer.getCurrentEquippedItem() != null && event.entityPlayer.getCurrentEquippedItem().getItem() == item.getEntityItem().getItem() && event.entityPlayer.getCurrentEquippedItem().getItemDamage() == item.getEntityItem().getItemDamage()) {
                        event.drops.remove(item);
                    }
                }
            }
        }
    } */
	
	}
