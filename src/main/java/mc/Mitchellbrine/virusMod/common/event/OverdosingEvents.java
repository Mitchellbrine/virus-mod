package mc.Mitchellbrine.virusMod.common.event;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class OverdosingEvents {

	@SubscribeEvent
	public void overdose(LivingUpdateEvent event) {
		
		// Overdosing
		
		if (event.entityLiving.isPotionActive(VirusMod.level1Medicine)) {
			if (event.entityLiving.getActivePotionEffect(VirusMod.level1Medicine).getDuration() >= 24000) {
				event.entityLiving.attackEntityFrom(VirusMod.overdose, 1.0F);
				event.entityLiving.addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 0));
			}
			
			if (event.entityLiving.getActivePotionEffect(VirusMod.level1Medicine).getDuration() == 0) {
				return;
			}
		}
		
		if (event.entityLiving.isPotionActive(VirusMod.level2Medicine)) {
			if (event.entityLiving.getActivePotionEffect(VirusMod.level2Medicine).getDuration() >= 12000) {
				event.entityLiving.attackEntityFrom(VirusMod.overdose, 1.0F);
				event.entityLiving.addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 0));				
			}
			
			if (event.entityLiving.getActivePotionEffect(VirusMod.level2Medicine).getDuration() == 0) {
				return;
			}
		}

        if (event.entityLiving.isPotionActive(VirusMod.parkinsonsMedicine)) {

            if (event.entityLiving.getActivePotionEffect(VirusMod.parkinsonsMedicine).getDuration() >= 12000) {
                event.entityLiving.attackEntityFrom(VirusMod.overdose, 1.0F);
                event.entityLiving.addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 0));
            }

            if (event.entityLiving.getActivePotionEffect(VirusMod.parkinsonsMedicine).getDuration() == 0) {
                return;
            }

        }
		
	}
	
}
