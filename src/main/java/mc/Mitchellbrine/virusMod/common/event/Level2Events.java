package mc.Mitchellbrine.virusMod.common.event;

import java.util.Random;

import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import mc.Mitchellbrine.virusMod.common.potion.Effects;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.client.Minecraft;
import net.minecraft.client.shader.ShaderGroup;
import net.minecraft.client.util.JsonException;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHelper;
import net.minecraft.stats.StatList;
import net.minecraft.util.*;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import scala.util.parsing.json.JSON;

public class Level2Events {

	@SubscribeEvent
	public void level2(LivingUpdateEvent event) {

		Random random = new Random();

        // 1.1 Disease Handling

        //if (event.entityLiving.worldObj.rand.nextInt(600) == 0) {
            //if (event.entityLiving instanceof EntityPlayer) {
                for (int i = 30; i < 40; i++) {
                    if (event.entityLiving.isPotionActive(i)) {
                        if (Potion.potionTypes[i] instanceof Disease) {
                            if (!DiseaseHandler.isDiseaseActive(event.entityLiving, i)) {
                                int diseaseLength = event.entityLiving.getActivePotionEffect(Potion.potionTypes[i]).getDuration();
                                event.entityLiving.getEntityData().setInteger("disease" + i, diseaseLength);
                                event.entityLiving.removePotionEffect(i);
                            }
                        }
                    }


                    if (event.entityLiving.getEntityData().hasKey("disease"+i)) {
                        if (event.entityLiving.getEntityData().getInteger("disease" + i) > 0) {
                            int diseaseLength = event.entityLiving.getEntityData().getInteger("disease" + i) - 1;
                            event.entityLiving.getEntityData().setInteger("disease" + i, diseaseLength);
                            if (VirusMod.useDebugDiseaseMessages == 1) {
                                if (!event.entityLiving.worldObj.isRemote) {
                                    VirusMod.debugLogger.info("Disease: " + i + " | Time Remaining: " + diseaseLength);
                                }
                            }
                            if (event.entityLiving.isPotionActive(i)) {
                                event.entityLiving.removePotionEffect(i);
                            }
                        } else if (event.entityLiving.getEntityData().getInteger("disease"+i) < 0){
                            event.entityLiving.getEntityData().setInteger("disease"+i,0);
                        }
                    }

                }

        for (int i = 80; i < 128; i++) {
            if (event.entityLiving.isPotionActive(i)) {
                if (Potion.potionTypes[i] instanceof Disease) {
                    if (!DiseaseHandler.isDiseaseActive(event.entityLiving, i)) {
                        int diseaseLength = event.entityLiving.getActivePotionEffect(Potion.potionTypes[i]).getDuration();
                        event.entityLiving.getEntityData().setInteger("disease" + i, diseaseLength);
                        event.entityLiving.removePotionEffect(i);
                    }
                }
            }


            if (event.entityLiving.getEntityData().hasKey("disease"+i)) {
                if (event.entityLiving.getEntityData().getInteger("disease" + i) > 0) {
                    int diseaseLength = event.entityLiving.getEntityData().getInteger("disease" + i) - 1;
                    event.entityLiving.getEntityData().setInteger("disease" + i, diseaseLength);
                    if (VirusMod.useDebugDiseaseMessages == 1) {
                        if (!event.entityLiving.worldObj.isRemote) {
                            VirusMod.debugLogger.info("Disease: " + i + " | Time Remaining: " + diseaseLength);
                        }
                    }
                    if (event.entityLiving.isPotionActive(i)) {
                        event.entityLiving.removePotionEffect(i);
                    }
                } else if (event.entityLiving.getEntityData().getInteger("disease"+i) < 0){
                    event.entityLiving.getEntityData().setInteger("disease"+i,0);
                }
            }

        }

           // }
        //}

		// Influenza
		
			if (DiseaseHandler.isDiseaseActive(event.entityLiving, VirusMod.influenza)) {
                if (event.entityLiving.getEntityData().getInteger("disease" + VirusMod.influenza.id) > 0) {
                    if (!event.entityLiving.isPotionActive(VirusMod.level1Medicine)) {
                        Effects.Symptoms.execute(event.entityLiving, VirusMod.influenza);
                    }
                }

                if (event.entityLiving.getEntityData().getInteger("disease" + VirusMod.influenza.id) == 1) {
                    if (event.entityLiving instanceof EntityPlayer) {

                        EntityPlayer player = (EntityPlayer) event.entityLiving;
                        player.inventory.addItemStackToInventory(new ItemStack(VirusMod.journalFlu, 1));

                    }
                }
            }
				
		// Malaria
				
				if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.malaria)) {
					if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.malaria.id) > 0) {
						if (!event.entityLiving.isPotionActive(VirusMod.level2Medicine)) {
                            Effects.Symptoms.execute(event.entityLiving,VirusMod.malaria);
						}
					}
					
					if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.malaria.id) == 1) {
                        Effects.Death.execute(random, event.entityLiving, 50);
					}
					
					
				}
				
				if (event.entityLiving.worldObj.getBiomeGenForCoords(new BlockPos((int)event.entityLiving.posX, (int)event.entityLiving.posY, (int)event.entityLiving.posX)).temperature >= 1.0F) {

                    if (event.entityLiving.worldObj.provider.getDimensionId() == 0) {
                        int malariaChance = random.nextInt(100);
                        int feverChance = random.nextInt(100);

                        if (malariaChance >= ((Disease) VirusMod.malaria).getChance()) {
                            if (!event.entityLiving.getEntityData().hasKey("vaccineMalaria") || !event.entityLiving.getEntityData().getBoolean("vaccineMalaria")) {
                                event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.malaria.id, 4800, 0, true,false));
                            }
                        }
                        if (feverChance >= ((Disease) VirusMod.yellowFever).getChance()) {
                            event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.yellowFever.id, 4800, 0, true,false));
                        }
                    }
				}
				
		// Yellow Fever
				
				
				if (DiseaseHandler.isDiseaseActive(event.entityLiving, VirusMod.yellowFever)) {
					if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.yellowFever.id) > 0) {
						if (!event.entityLiving.isPotionActive(VirusMod.level2Medicine)) {
						Effects.Symptoms.execute(event.entityLiving, VirusMod.yellowFever);
						}
					}
					
					if (event.entityLiving.getEntityData().getInteger("disease"+VirusMod.yellowFever.id) == 1) {

                        Effects.Death.execute(random, event.entityLiving, 50);

						return;
					}
					
				}
				
				
		// Pneumonic Plague
				
				if (DiseaseHandler.isDiseaseActive(event.entityLiving, VirusMod.pPlague)) {
                    Effects.Symptoms.execute(event.entityLiving, VirusMod.pPlague);


                    if (event.entityLiving.getEntityData().getInteger("disease" + VirusMod.pPlague.id) == 1) {
                        Effects.Death.execute(random, event.entityLiving, 50);
                    }
                }
	}
	
	@SubscribeEvent
	public void level3(LivingUpdateEvent event) {
		
		Random random = new Random();
		
		// Parkinson's Disease
		
		if (event.entityLiving instanceof EntityPlayer) {
			EntityPlayer player = (EntityPlayer) event.entityLiving;
			
			if (player instanceof EntityPlayerMP) {
				EntityPlayerMP playerMP = (EntityPlayerMP) player;
				
				if (playerMP.getStatFile().writeStat(StatList.distanceWalkedStat) >= 1000000) {
					int parkinsonChance = random.nextInt(100);
											
					if (parkinsonChance <= ((Disease)VirusMod.parkinsons).getChance()) {
						if (!player.isPotionActive(VirusMod.parkinsons)) {
                            if (player.getEntityData().hasKey("gottenParkinsons") && !player.getEntityData().getBoolean("gottenParkinsons")) {
                                player.addPotionEffect(new PotionEffect(VirusMod.parkinsons.id, 2400, 0, true,false));
                            }
						}
					}
				}
				
			}
		}
		
		if (DiseaseHandler.isDiseaseActive(event.entityLiving,VirusMod.parkinsons)) {
			if (event.entityLiving instanceof EntityPlayer) {
                EntityPlayer player = (EntityPlayer) event.entityLiving;

                    int dropChance = random.nextInt(3000);
                    if (dropChance == 0) {
                        Effects.Fumbling.execute(player);
                    }
                    Effects.Jitter.execute(random, player);
            }
		}

        // Shader Potion

        if (VirusMod.shaderPlague != null) {
            if (event.entityLiving.isPotionActive(VirusMod.shaderPlague)) {

                if (event.entityLiving instanceof EntityPlayer) {

                    EntityPlayer player = (EntityPlayer) event.entityLiving;

                    Effects.Seizure.execute(random, player);

                }
                }
                else {
                if (event.entityLiving instanceof EntityPlayer && ((EntityPlayer)event.entityLiving).worldObj.isRemote) {
                            //Minecraft.getMinecraft().entityRenderer.deact();
                }
            }

        }

        // Flowstone Fever

        if (Loader.isModLoaded("flowstonemod")) {
            if (event.entityLiving.isPotionActive(VirusMod.flowstoneFever)) {

                if (event.entityLiving instanceof EntityPlayer) {

                 EntityPlayer player = (EntityPlayer) event.entityLiving;

                   Effects.FlowStone.execute(player);

                }

            }

            if (random.nextInt(500000) <= 10 && !event.entityLiving.isPotionActive(VirusMod.flowstoneFever) && Loader.isModLoaded("flowstonemod")) {

               event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.flowstoneFever.id, 2400, 0, true,false));

            }


        }

        // Astigmatism

        if (event.entityLiving instanceof EntityPlayer) {

            EntityPlayer player = (EntityPlayer) event.entityLiving;

           if (DiseaseHandler.isDiseaseActive(player, VirusMod.astigmatism)) {
               if (player.worldObj.isRemote) {
                   Effects.Seizure.setShader(new ResourceLocation("minecraft:shaders/post/deconverge.json"));

               }
               if (player.getEntityData().getInteger("disease" + VirusMod.astigmatism.id) == 1) {
                   if (random.nextInt(100) > 60) {
                       player.addPotionEffect(new PotionEffect(Potion.blindness.id, 72000, 4, true,false));
                   }
                   if (player.worldObj.isRemote) {
                       //Minecraft.getMinecraft().entityRenderer.deactivateShader();
                   }

                   player.getEntityData().setBoolean("gottenAstigmatism", true);
               }
           }


            // Getting astigmatism

            if (player instanceof EntityPlayerMP) {
                EntityPlayerMP playerMP = (EntityPlayerMP) player;

                if (playerMP.getStatFile().writeStat(StatList.minutesPlayedStat) >= 100000000) {
                    if (!player.getEntityData().hasKey("gottenAstigmatism") || !player.getEntityData().getBoolean("gottenAstigmatism")) {
                        if (random.nextInt(10000) <= ((Disease)VirusMod.astigmatism).getChance()) {
                            player.addPotionEffect(new PotionEffect(VirusMod.astigmatism.id, 2400, 0, true,false));
                        }
                    }
                }
            }

        }

        // Sleep in Bed

        if (event.entityLiving instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) event.entityLiving;
            if (!((EntityPlayer) event.entityLiving).worldObj.isRemote) {
                if (VirusMod.sleepLocation == 0) {
                    if (player.isPlayerFullyAsleep()) {
                        if (DiseaseHandler.isDiseaseActive(player, VirusMod.influenza)) {
                            player.addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "You sleep and your sickness vanishes!"));
                            if (DiseaseHandler.removeDisease(player,VirusMod.influenza.id)) {
                            }
                        }
                    }
                }
                else if (VirusMod.sleepLocation == 1) {
                    if (Minecraft.getMinecraft().isSingleplayer() && player.isPlayerFullyAsleep()) {
                        if (DiseaseHandler.isDiseaseActive(player, VirusMod.influenza)) {
                            player.addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "You sleep and your sickness vanishes!"));
                            if (DiseaseHandler.removeDisease(player,VirusMod.influenza.id)) {
                            }
                        }
                    }
                }
                else if (VirusMod.sleepLocation == 2) {
                    if (!Minecraft.getMinecraft().isSingleplayer() && player.isPlayerFullyAsleep()) {
                        if (DiseaseHandler.isDiseaseActive(player, VirusMod.influenza)) {
                            player.addChatMessage(new ChatComponentText(EnumChatFormatting.GRAY + "" + EnumChatFormatting.ITALIC + "You sleep and your sickness vanishes!"));
                            if (DiseaseHandler.removeDisease(player,VirusMod.influenza.id)) {
                            }
                        }
                    }
                }
            }
        }


	}


    @SubscribeEvent
    public void deathParkinsons(LivingDeathEvent event) {

    if (event.entityLiving instanceof EntityPlayer) {
        EntityPlayer player = (EntityPlayer) event.entityLiving;

        if (player.getEntityData().hasKey("gottenParkinsons")) {
            if (player.isPotionActive(VirusMod.parkinsons)) {
                player.getEntityData().setBoolean("gottenParkinsons", true);
            }
        }

        if (player.getEntityData().hasKey("gottenAstigmatism")) {
            if (player.isPotionActive(Potion.blindness)) {
                player.getEntityData().setBoolean("gottenAstigmatism",true);
            }
        }
    }

    }

    @SubscribeEvent
    public void damage(LivingHurtEvent event) {

        Random random = new Random();

        if (event.entityLiving instanceof EntityPlayer) {

            if (event.source == DamageSource.starve) {
                if (random.nextInt(1000) <= 10) {
                    if (VirusMod.shaderPlague != null) {
                        event.entityLiving.addPotionEffect(new PotionEffect(VirusMod.shaderPlague.id, 200, 0, true,false));
                    }
                }
            }

        }
    }

    @SubscribeEvent
    public void join(EntityJoinWorldEvent event) {

    Random random = new Random();

        if (event.entity instanceof EntityPlayer) {

        EntityPlayer player = (EntityPlayer) event.entity;
        if (!player.getEntityData().hasKey("gottenAstigmatism") || !player.getEntityData().getBoolean("gottenAstigmatism")) {
            if (random.nextInt(100) <= 5) {
                VirusMod.logger.info("Player (" + player.getName() + ") has Astigmatism");
            }
        }

        }


    }

}
