package mc.Mitchellbrine.virusMod.common.entity.player;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;

public class ParkinsonsYOGO implements IExtendedEntityProperties {


    public final static String EXT_PROP_NAME = "DiseasedPlayer";

    private final EntityPlayer player;


    private boolean hasGottenParkinsons;
    private boolean hasGottenAstigmatism;


    public ParkinsonsYOGO(EntityPlayer player) {
        this.player = player;
        this.hasGottenParkinsons = false;
        this.hasGottenAstigmatism = false;
    }


    public static final void register(EntityPlayer player) {
        player.registerExtendedProperties(ParkinsonsYOGO.EXT_PROP_NAME, new ParkinsonsYOGO(player));
    }


    public static final ParkinsonsYOGO get(EntityPlayer player) {
        return (ParkinsonsYOGO) player.getExtendedProperties(EXT_PROP_NAME);
    }

    @Override
    public void saveNBTData(NBTTagCompound compound) {
        NBTTagCompound properties = new NBTTagCompound();

        properties.setBoolean("gottenParkinsons",hasGottenParkinsons);
        properties.setBoolean("gottenAstigmatism",hasGottenAstigmatism);

        compound.setTag(EXT_PROP_NAME, properties);

    }

    // Load whatever data you saved
    @Override
    public void loadNBTData(NBTTagCompound compound) {
        NBTTagCompound properties = (NBTTagCompound) compound.getTag(EXT_PROP_NAME);
        this.hasGottenParkinsons = properties.getBoolean("gottenParkinsons");
        this.hasGottenAstigmatism = properties.getBoolean("gottenAstigmatism");
    }

    @Override
    public void init(Entity entity, World world) {

    }
}
