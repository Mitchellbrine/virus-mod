package mc.Mitchellbrine.virusMod.common.entity.doctor;


import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Items;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.world.World;

import java.util.Random;

public class EntityDoctor extends EntityCreature{

    Random random = new Random();

    public EntityDoctor(World par1World, int profession) {
        super(par1World);
        this.setProfession(profession);
        String villagerName = "";
        if (this.getProfession() != 3) {
            switch(random.nextInt(43)) {
                case 0: villagerName = "Dr. Thomasson"; break;
                case 1: villagerName = "Dr. Edmonds"; break;
                case 2: villagerName = "Dr. Davidson"; break;
                case 3: villagerName = "Dr. Davis"; break;
                case 4: villagerName = "Dr. Scarlinsky"; break;
                case 5: villagerName = "Dr. Bohage"; break;
                case 6: villagerName = "Dr. Gillfilling"; break;
                case 7: villagerName = "Dr. Paxter"; break;
                case 8: villagerName = "Dr. Patel"; break;
                case 9: villagerName = "Dr. Lemmons"; break;
                case 10: villagerName = "Dr. Hubbard"; break;
                case 11: villagerName = "Dr. Raymond"; break;
                case 12: villagerName = "Dr. Carlise"; break;
                case 13: villagerName = "Dr. Hudson"; break;
                case 14: villagerName = "Dr. Preston"; break;
                case 15: villagerName = "Dr. Wentworth"; break;
                case 16: villagerName = "Dr. Kennedy"; break;
                case 17: villagerName = "Dr. Pedelecki"; break;
                case 18: villagerName = "Dr. Conway"; break;
                case 19: villagerName = "Dr. Achard"; break;
                case 20: villagerName = "Dr. Thomas"; break;
                case 21: villagerName = "Dr. Atterson"; break;
                case 22: villagerName = "Dr. Hansen"; break;
                case 23: villagerName = "Dr. Beckmann"; break;
                case 24: villagerName = "Dr. Williams"; break;
                case 25: villagerName = "Dr. Mills"; break;
                case 26: villagerName = "Dr. Isom"; break;
                case 27: villagerName = "Dr. Smith"; break;
                case 28: villagerName = "Dr. Coder"; break;
                case 29: villagerName = "Dr. Huntley"; break;
                case 30: villagerName = "Dr. Mort"; break;
                case 31: villagerName = "Dr. Rob Reid"; break;
                case 32: villagerName = "Doctor Octagonapus"; break;
                case 33: villagerName = "Doctor"; break;
                case 34: villagerName = "A Doctor"; break;
                case 35: villagerName = "Dr. Petal"; break;
                case 36: villagerName = "Dr. Dolittle"; break;
                case 37: villagerName = "Dr. Phil"; break;
                case 38: villagerName = "Dr. entity.dr.name"; break;
                case 39: villagerName = "Dr. Dinnerbone"; break;
                case 40: villagerName = "Dr. Sore N. Chests"; break;
                case 41: villagerName = "Dr. Turtle"; break;
                case 42: villagerName = "a8bf34cc-242f-4619-a054-e40559cf8244"; break;
            }
        } else {
            villagerName = "The Doctor";
        }
        this.setDoctorName(villagerName);
    }

    public EntityDoctor(World par1World) {
        super(par1World);
        if (this.getProfession() == 0) {
            this.setProfession(random.nextInt(4));
        }
        if (this.getDoctorCost() == 0) {
            switch(this.getProfession()) {
                case 0: this.setDoctorCost(random.nextInt(10)); break;
                case 1: this.setDoctorCost(random.nextInt(15)); break;
                case 2: this.setDoctorCost(random.nextInt(30)); break;
                case 3: this.setDoctorCost(random.nextInt(30)); break;
            }
        }
        String villagerName = "";
        if (this.getProfession() != 3) {
            switch(random.nextInt(43)) {
                case 0: villagerName = "Dr. Thomasson"; break;
                case 1: villagerName = "Dr. Edmonds"; break;
                case 2: villagerName = "Dr. Davidson"; break;
                case 3: villagerName = "Dr. Davis"; break;
                case 4: villagerName = "Dr. Scarlinsky"; break;
                case 5: villagerName = "Dr. Bohage"; break;
                case 6: villagerName = "Dr. Gillfilling"; break;
                case 7: villagerName = "Dr. Paxter"; break;
                case 8: villagerName = "Dr. Patel"; break;
                case 9: villagerName = "Dr. Lemmons"; break;
                case 10: villagerName = "Dr. Hubbard"; break;
                case 11: villagerName = "Dr. Raymond"; break;
                case 12: villagerName = "Dr. Carlise"; break;
                case 13: villagerName = "Dr. Hudson"; break;
                case 14: villagerName = "Dr. Preston"; break;
                case 15: villagerName = "Dr. Wentworth"; break;
                case 16: villagerName = "Dr. Kennedy"; break;
                case 17: villagerName = "Dr. Pedelecki"; break;
                case 18: villagerName = "Dr. Conway"; break;
                case 19: villagerName = "Dr. Achard"; break;
                case 20: villagerName = "Dr. Thomas"; break;
                case 21: villagerName = "Dr. Atterson"; break;
                case 22: villagerName = "Dr. Hansen"; break;
                case 23: villagerName = "Dr. Beckmann"; break;
                case 24: villagerName = "Dr. Williams"; break;
                case 25: villagerName = "Dr. Mills"; break;
                case 26: villagerName = "Dr. Isom"; break;
                case 27: villagerName = "Dr. Smith"; break;
                case 28: villagerName = "Dr. Coder"; break;
                case 29: villagerName = "Dr. Huntley"; break;
                case 30: villagerName = "Dr. Mort"; break;
                case 31: villagerName = "Dr. Rob Reid"; break;
                case 32: villagerName = "Doctor Octagonapus"; break;
                case 33: villagerName = "Doctor"; break;
                case 34: villagerName = "A Doctor"; break;
                case 35: villagerName = "Dr. Petal"; break;
                case 36: villagerName = "Dr. Dolittle"; break;
                case 37: villagerName = "Dr. Phil"; break;
                case 38: villagerName = "Dr. entity.dr.name"; break;
                case 39: villagerName = "Dr. Dinnerbone"; break;
                case 40: villagerName = "Dr. Sore N. Chests"; break;
                case 41: villagerName = "Dr. Turtle"; break;
                case 42: villagerName = "a8bf34cc-242f-4619-a054-e40559cf8244"; break;
            }
        } else {
            villagerName = "The Doctor";
        }
        this.setDoctorName(villagerName);
    }

    protected void entityInit() {
        super.entityInit();
        this.dataWatcher.addObject(16, Integer.valueOf(0));
        this.dataWatcher.addObject(17,"");
        this.dataWatcher.addObject(18, Integer.valueOf(0));
    }

    @Override
    public boolean getCanSpawnHere()
    {
        return worldObj.villageCollectionObj.getVillageList().iterator().hasNext() && worldObj.villageCollectionObj.func_176056_a(new BlockPos((int)this.posX, (int)this.posY, (int)this.posZ), 10) != null;
    }

    public int getProfession() {
        return this.dataWatcher.getWatchableObjectInt(16);
    }

    public String getDoctorName() {
        return this.dataWatcher.getWatchableObjectString(17);
    }

    public void setProfession(int profession) {
        this.dataWatcher.updateObject(16,Integer.valueOf(profession));
    }

    public void setDoctorName(String name) {
        this.dataWatcher.updateObject(17,name);
    }

    public void setDoctorCost(int cost) {
        this.dataWatcher.updateObject(18,cost);
    }

    public int getDoctorCost() { return this.dataWatcher.getWatchableObjectInt(18); }

    protected boolean canDespawn()
    {
        return true;
    }

    public void writeEntityToNBT(NBTTagCompound nbt) {
        super.writeEntityToNBT(nbt);
        nbt.setInteger("Profession",this.getProfession());
        if (!this.getDoctorName().equals("")) {
            nbt.setString("Name", this.getDoctorName());
        }
        nbt.setInteger("Cost",this.getDoctorCost());
    }

    public void readEntityFromNBT(NBTTagCompound nbt) {
        super.readEntityFromNBT(nbt);
        if (nbt.hasKey("Profession")) {
            this.setProfession(nbt.getInteger("Profession"));
        }
        if (nbt.hasKey("Name")) {
            this.setDoctorName(nbt.getString("Name"));
        }
        if (nbt.hasKey("Cost")) {
            this.setDoctorCost(nbt.getInteger("Cost"));
        }
    }

    /**
     * Returns the sound this mob makes while it's alive.
     */
    protected String getLivingSound()
    {
        return this.getProfession() != 3 ? "mob.villager.idle" : "VirusMod:mob_doctor_idle";
    }

    /**
     * Returns the sound this mob makes when it is hurt.
     */
    protected String getHurtSound()
    {
        return this.getProfession() != 3 ? "mob.villager.hit" : null;
    }

    /**
     * Returns the sound this mob makes on death.
     */
    protected String getDeathSound()
    {
        return this.getProfession() != 3 ? "mob.villager.death" : null;
    }

    /**
     * Called when a player interacts with a mob. e.g. gets milk from a cow, gets into the saddle on a pig.
     */
    public boolean interact(EntityPlayer player)
    {
        if (player.getCurrentEquippedItem() == null) {
                if (!player.worldObj.isRemote) {
                    if (this.getProfession() != 3) {
                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.dialogue.hello", this.getDoctorName(),this.getDoctorCost()));
                    } else {
                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.dialogue.hello.who", this.getDoctorName(),this.getDoctorCost()));
                    }
                }
            return true;
        } else {
            if (player.getCurrentEquippedItem().getItem() == Items.emerald && player.getCurrentEquippedItem().stackSize >= this.getDoctorCost()) {
                int diseasesAmount = 0;
                for(int i = 30; i < 40; i++) {
                    if (DiseaseHandler.isDiseaseActive(player,i)) {
                        if ((this.getProfession() + 1) >= VirusMod.diseaseLevel.get(Potion.potionTypes[i])) {
                            if (!player.worldObj.isRemote) {
                                if (Potion.potionTypes[i].getName().substring(8).toUpperCase().equals("PNEUMONICPLAGUE")) {
                                    player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.found", "PNEUMONIC PLAGUE"));
                                    if (VirusMod.diseaseLevel.get(Potion.potionTypes[i]) < 3) {
                                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.vaccine","PNEUMONIC PLAGUE"));
                                    }
                                } else if (Potion.potionTypes[i].getName().substring(8).toUpperCase().equals("BUBONICPLAGUE")){
                                    player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.found", "BUBONIC PLAGUE"));
                                    if (VirusMod.diseaseLevel.get(Potion.potionTypes[i]) < 3) {
                                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.vaccine","BUBONIC PLAGUE"));
                                    }
                                } else if (Potion.potionTypes[i].getName().substring(8).toUpperCase().equals("YELLOWFEVER")) {
                                    player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.found", "YELLOW FEVER"));
                                    if (VirusMod.diseaseLevel.get(Potion.potionTypes[i]) < 3) {
                                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.vaccine", "YELLOW FEVER"));
                                    }
                                } else if (Potion.potionTypes[i].getName().substring(8).toUpperCase().equals("HEAT")) {
                                    player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.found", "HEAT STROKE"));
                                } else {
                                    player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.found", Potion.potionTypes[i].getName().substring(8).toUpperCase()));
                                    if (VirusMod.diseaseLevel.get(Potion.potionTypes[i]) < 3) {
                                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.found.vaccine",Potion.potionTypes[i].getName().substring(8).toUpperCase()));
                                    }
                                }
                                diseasesAmount++;
                            }
                        }
                    }
                }
                if (!player.worldObj.isRemote) {
                    if (diseasesAmount >= 3) {
                        player.addChatComponentMessage(new ChatComponentTranslation("doctor.disease.many"));
                    }
                    switch (this.getProfession()) {
                        case 0:
                            player.addChatComponentMessage(new ChatComponentTranslation("doctor.dialogue.finish.low", this.getDoctorName()));
                            break;
                        case 1:
                            player.addChatComponentMessage(new ChatComponentTranslation("doctor.dialogue.finish.low", this.getDoctorName()));
                            break;
                        case 2:
                            player.addChatComponentMessage(new ChatComponentTranslation("doctor.dialogue.finish", this.getDoctorName()));
                            break;
                        case 3:
                            player.addChatComponentMessage(new ChatComponentTranslation("doctor.dialogue.finish.doctor", this.getDoctorName()));
                            break;
                    }
                }

                for (int i = 0; i < this.getDoctorCost();i++) {
                    player.getCurrentEquippedItem().stackSize--;
                }

            }
        }
        return false;
    }

}
