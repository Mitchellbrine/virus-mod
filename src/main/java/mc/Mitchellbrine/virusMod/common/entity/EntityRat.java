package mc.Mitchellbrine.virusMod.common.entity;

import java.util.Random;

import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import net.minecraft.entity.Entity;
import net.minecraft.entity.SharedMonsterAttributes;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class EntityRat extends EntitySilverfish{

	public EntityRat(World par1World) {
		super(par1World);
	}
	
    protected void applyEntityAttributes()
    {
        super.applyEntityAttributes();
        this.getEntityAttribute(SharedMonsterAttributes.maxHealth).setBaseValue(8.0D);
        this.getEntityAttribute(SharedMonsterAttributes.movementSpeed).setBaseValue(0.6000000238418579D);
        this.getEntityAttribute(SharedMonsterAttributes.attackDamage).setBaseValue(0.0F);
    }

    protected void attackEntity(Entity par1Entity, float par2)
    {
    	Random random = new Random();
    	
    	int randomChance = random.nextInt(((Disease)VirusMod.bubonicPlague).getChance());
    	if (par1Entity.getBoundingBox().maxY > this.getBoundingBox().minY && par1Entity.getBoundingBox().minY < this.getBoundingBox().maxY) {
    	if (randomChance == 0) {
    		if (par1Entity instanceof EntityPlayer) {
    			EntityPlayer player = (EntityPlayer)par1Entity;
    			
    			if (VirusMod.enableDebug == 1) {
    				if (!player.isPotionActive(VirusMod.bubonicPlague)) {
    					player.addPotionEffect(new PotionEffect(VirusMod.bubonicPlague.id, 9600, 0, true,false));
    				}
    			}
    			else {
    				if (!player.isPotionActive(VirusMod.bubonicPlague)) {
    					player.addPotionEffect(new PotionEffect(VirusMod.bubonicPlague.id, 24000, 0, true,false));
    				}
    			}
    		}
    	}
    }
    }
    
    

}
