package mc.Mitchellbrine.virusMod.common.entity.villager;

import net.minecraftforge.fml.common.registry.VillagerRegistry;
import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.entity.passive.EntityVillager;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.village.MerchantRecipe;
import net.minecraft.village.MerchantRecipeList;

import java.util.Random;

public class MedicationTrading /*implements VillagerRegistry.*/ {
    /*
    @Override
    public void manipulateTradesForVillager(EntityVillager villager, MerchantRecipeList recipeList, Random random) {
        if (random.nextInt() > 80) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(5)), new ItemStack(VirusMod.tylenol, 1, 0)));
        }

        if (random.nextInt() < 80) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(10)), new ItemStack(VirusMod.chloroquine, 1, 0)));
        }

        if (random.nextInt() < 10) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(15)), new ItemStack(VirusMod.carbodopa, random.nextInt(3), 0)));
        }

        if (random.nextInt() > 90) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(3)), new ItemStack(VirusMod.influenzaVaccine,1)));
        }

        if (random.nextInt() > 95) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(6)), new ItemStack(VirusMod.rabiesVaccine,1)));
        }

        if (random.nextInt() > 97) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(15)), new ItemStack(VirusMod.malariaVaccine,1)));
        }

        if (random.nextInt() > 99) {
            recipeList.add(new MerchantRecipe(new ItemStack(Items.emerald, random.nextInt(30)), new ItemStack(VirusMod.pneumonicVaccine,1)));
        }

    } */
}
