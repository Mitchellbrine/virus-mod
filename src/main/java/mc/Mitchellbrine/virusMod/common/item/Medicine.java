package mc.Mitchellbrine.virusMod.common.item;

import java.util.List;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;

public class Medicine extends Item{

	private int strengthLevel;
	
	public Medicine(int strength) {
		strengthLevel = strength;
		this.setCreativeTab(CreativeTabs.tabBrewing);
		//this.setTextureName("virusMod:medicine");
        VirusMod.items.add(this);
	}

	@Override
	public int getMaxItemUseDuration(ItemStack itemstack) {
		return 32;
	}

	@Override
	public EnumAction getItemUseAction(ItemStack itemstack) {
		return EnumAction.EAT;
	}

	@Override
	public ItemStack onItemUseFinish(ItemStack itemstack, World world, EntityPlayer player) {

        switch(strengthLevel) {
            case 1:
                if (player.isPotionActive(VirusMod.level1Medicine)) {
                    player.addPotionEffect(new PotionEffect(VirusMod.level1Medicine.id, player.getActivePotionEffect(VirusMod.level1Medicine).getDuration() + 6000, 0, true,false));
                }
                else {
                    player.addPotionEffect(new PotionEffect(VirusMod.level1Medicine.id, 6000, 0, true,false));
                } break;
            case 2:
                if (player.isPotionActive(VirusMod.level2Medicine)) {
                    player.addPotionEffect(new PotionEffect(VirusMod.level2Medicine.id, player.getActivePotionEffect(VirusMod.level2Medicine).getDuration() + 3000, 0, true,false));
                }
                else {
                    player.addPotionEffect(new PotionEffect(VirusMod.level2Medicine.id, 6000, 0, true,false));
                } break;
            case 3:
                if (player.isPotionActive(VirusMod.parkinsonsMedicine)) {
                    player.addPotionEffect(new PotionEffect(VirusMod.parkinsonsMedicine.id, player.getActivePotionEffect(VirusMod.parkinsonsMedicine).getDuration() + 3000, 0, true,false));
                }
                else {
                    player.addPotionEffect(new PotionEffect(VirusMod.parkinsonsMedicine.id, 3000, 0, true,false));
                } break;
            case -1:
                player.attackEntityFrom(VirusMod.overdose, 100.0F); break;
            case -2:
                player.worldObj.createExplosion(null, player.posX, player.posY, player.posZ, 2.0F, false); break;
        }
		super.onItemUseFinish(itemstack, world, player);
		return itemstack;
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public void addInformation(ItemStack itemstack, EntityPlayer player, List list, boolean par4) {
		list.add("Medicine Type: " + this.getUnlocalizedName().substring(5));
	}
	
	@Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
            par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));

        return par1ItemStack;
    }
	
	public int getStrength(Medicine medicine) {
		return medicine.strengthLevel;
	}
	
}
