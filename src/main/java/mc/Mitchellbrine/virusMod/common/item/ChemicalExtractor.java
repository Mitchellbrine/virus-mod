package mc.Mitchellbrine.virusMod.common.item;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import net.minecraft.block.state.IBlockState;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;

import java.util.List;

public class ChemicalExtractor extends Item {

    public ChemicalExtractor() {
        super();
        setMaxStackSize(1);
        setCreativeTab(VirusMod.warfareTab);
        this.setHasSubtypes(true);
            //registerPotion();
        GameRegistry.registerItem(this, "chemicalExtractor");
        VirusMod.items.add(this);

    }

    public boolean onLeftClickEntity(ItemStack itemStack, EntityPlayer entityLivingBase, Entity entityLivingBase2) {
        if (itemStack.getItemDamage() > 0) {
            if (Potion.potionTypes[itemStack.getItemDamage()] != null) {
                if (Potion.potionTypes[itemStack.getItemDamage()] instanceof Disease) {
                    if (((Disease)Potion.potionTypes[itemStack.getItemDamage()]).isEnabled()) {
                        if (!(entityLivingBase2 instanceof EntityPlayer)) {
                            entityLivingBase2.getEntityData().setInteger("disease" + itemStack.getItemDamage(), 1200);
                            entityLivingBase.setCurrentItemOrArmor(0, new ItemStack(VirusMod.chemicalExtractor, 1, 0));
                            return true;
                        } else {
                            entityLivingBase2.getEntityData().setInteger("disease" + itemStack.getItemDamage(), 1200);
                            entityLivingBase.setCurrentItemOrArmor(0, new ItemStack(VirusMod.chemicalExtractor, 1, 0));
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    /**
     * Called each tick as long the item is on a player inventory. Uses by maps to check if is on a player hand and
     * update it's contents.
     */
    @Override
    public void onUpdate(ItemStack stack, World world, Entity entity, int par4, boolean par5)
    {
        if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
        }
    }
    /**
     * Called when item is crafted/smelted. Used only by maps so far.
     */
    @Override
    public void onCreated(ItemStack stack, World world, EntityPlayer player)
    {
     if (!stack.hasTagCompound()) {
         stack.setTagCompound(new NBTTagCompound());
     }

    }
    /**
     * called when the player releases the use item button. Args: itemstack, world, entityplayer, itemInUseCount
     */
    @Override
    public void onPlayerStoppedUsing(ItemStack stack, World world, EntityPlayer player, int itemInUseCount)
    {

    }
    /**
     * allows items to add custom lines of information to the mouseover description
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List lore, boolean par4)
    {
        if (Potion.potionTypes[stack.getItemDamage()] != null) {
            lore.add("Disease: " + StatCollector.translateToLocal(Potion.potionTypes[stack.getItemDamage()].getName()));
        }
    }
    /**
     * Returns true if players can use this item to affect the world (e.g. placing blocks, placing ender eyes in portal)
     * when not in creative
     */
    @Override
    public boolean canItemEditBlocks()
    {
        return false;

    }

/*    @Override
    public String getPotionEffect(ItemStack is){
        if (is.getTagCompound().hasKey("chemical") && is.getTagCompound().getString("chemical") == "Yersinia pestis") {
            return "+0+1-2+3+10+13";
        }
        return "";
    } */

    /*public void registerPotion(){
        try{
            Class potHepler = PotionHelper.class;
            Field potFields[] = potHepler.getDeclaredFields();
            Field mods;
            mods = Field.class.getDeclaredField("modifiers");
            mods.setAccessible(true);
            for(Field f : potFields){
                if(f.getName() == "potionRequirements" || f.getName() == "field_77927_l"){
                    mods.setInt(f, f.getModifiers() & ~Modifier.FINAL);
                    f.setAccessible(true);
                    final HashMap<Integer,String> newReq = (HashMap<Integer,String>)((HashMap<Integer,String>)f.get(null)).clone();
                    for (int i = 0; i < Potion.potionTypes.length; i++) {
                        if (VirusMod.diseases.contains(Potion.potionTypes[i])) {
                            newReq.put(Integer.valueOf(i), " 0 & 1 & !2 & 3 & 4 & 10 & 3+6");
                        }
                    }
                    f.set(null, newReq);
                } else {
				}
            }
        }catch (Exception e){
            VirusMod.logger.error("Something went wrong while registering potions!");
            e.printStackTrace();
        }

    } */

    @Override
    @SuppressWarnings("unchecked")
    public void getSubItems(Item item, CreativeTabs tab, List l) {
        l.add(new ItemStack(this, 1, 0));
        /*l.add(new ItemStack(Items.potionitem,1,9243));
        l.add(new ItemStack(Items.potionitem,1,9307));
        l.add(new ItemStack(Items.potionitem,1,17435));
        l.add(new ItemStack(Items.potionitem,1,17499)); */
        for (int i = 30; i < 40;i++) {
            if (Potion.potionTypes[i] != null) {
                l.add(new ItemStack(this,1,i));
            }
        }

    }

    /*@SideOnly(Side.CLIENT)
    public IIcon getIconFromDamage(int meta) {
        if (meta == 0) {
            return icon[meta];
        } else {
            return icon[1];
        }
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister ri) {
        icon[0] = ri.registerIcon("VirusMod:syringe");
        icon[1] = ri.registerIcon("VirusMod:syringeFull");
    } */

    @Override
    public int getMaxItemUseDuration(ItemStack itemstack) {
        return 64;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack itemstack) {
        return EnumAction.BOW;
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack itemstack, World world, EntityPlayer player) {
        if (itemstack.getItemDamage() > 0) {
            if (Potion.potionTypes[itemstack.getItemDamage()] != null) {
                if (Potion.potionTypes[itemstack.getItemDamage()] instanceof Disease) {
                    if (((Disease) Potion.potionTypes[itemstack.getItemDamage()]).isEnabled()) {
                        player.getEntityData().setInteger("disease" + itemstack.getItemDamage(), 1200);
                        player.setCurrentItemOrArmor(0, new ItemStack(VirusMod.chemicalExtractor, 1, 0));
                    }
                }
            }
        }
        return itemstack;
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par1ItemStack.getItemDamage() > 0) {
            par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        }
        return par1ItemStack;
    }

    @Override
    public boolean onItemUse(ItemStack itemStack, EntityPlayer player, World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (itemStack.getItemDamage() == 0) {
            if (world.getBlockState(pos).getBlock() == Blocks.leaves2) {
                if (world.getBiomeGenForCoords(pos).temperature > 1.0F) {
                    if (itemRand.nextInt(100) < 10) {
                        itemStack.setItemDamage(VirusMod.malaria.id);
                    }
                }
            }

            if (world.getBlockState(pos).getBlock() == Blocks.grass) {
                if (world.getBiomeGenForCoords(pos).temperature > 1.0F) {
                    if (itemRand.nextInt(100) < 8) {
                        itemStack.setItemDamage(VirusMod.yellowFever.id);
                    }
                }
            }

        }
        return true;
    }


}
