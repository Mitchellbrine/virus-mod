package mc.Mitchellbrine.virusMod.common.item;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemEditableBook;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.world.World;


public class MedicalJournal extends ItemEditableBook {

    private boolean isFlu;
    private boolean isRabies;
    private boolean isPneumonic;
    private boolean isMalar;
    private boolean isBlackDeath;
    private boolean isParkinsons;
    private boolean isFever;
    private boolean isMeds;
    private boolean isBio;
    private boolean isVaccine;

    public MedicalJournal() {
        super();
        isFlu = true;
        isRabies = true;
        isPneumonic = true;
        isMalar = true;
        isBlackDeath = true;
        isParkinsons = true;
        isFever = true;
        isMeds = true;
        isBio = true;
        isVaccine = true;
        //this.setTextureName("book_written");
        VirusMod.items.add(this);

    }

    public MedicalJournal(boolean par1, boolean par2, boolean par3, boolean par4, boolean par5, boolean par6, boolean par7, boolean par8, boolean par9,boolean par10) {
        super();

        isFlu = par1;
        isRabies = par2;
        isPneumonic = par3;
        isMalar = par4;
        isBlackDeath = par5;
        isParkinsons = par6;
        isFever = par7;
        isMeds = par8;
        isBio = par9;
        isVaccine = par10;
        VirusMod.items.add(this);
        //this.setTextureName("book_written");
    }


    public NBTTagList fluPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.GREEN + "Influenza:\n\n-Symptoms: Nausea, Weakness, Drowsiness, Fatigue\n-Transferred from: PLAYERS, ZOMBIES\n-Treatable With: Acetaminophen"));
        return bookTagList;
    }

    public NBTTagList rabiesPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.GREEN + "Rabies:\n\n-Symptoms: Hydrophobia, Slowness\n-Transferred from: WOLVES\nMortality Rate: 50%\n-Treatable With: Acetaminophen"));
        return bookTagList;
    }

    public NBTTagList pneumonicPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.GOLD + "Pneumonic Plague:\n\n-Symptoms: Weakness, Drowsiness\n-Transferred from: RAW ANIMAL MEAT\n-Mortality Rate: 50%\n-Treatable With: Chloroquine"));
        return bookTagList;
    }

    public NBTTagList malariaPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.GOLD + "Malaria:\n\n-Symptoms: Weakness, Fatigue\n-Transferred from: MOSQUITOES (Hot Climate)\n-Mortality Rate: 50%\n-Treatable With: Chloroquine"));
        return bookTagList;
    }

    public NBTTagList blackDeathPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.DARK_RED + "Bubonic Plague:\n\n-Symptoms: Weakness, Drowsiness, Fatigue\n-Transferred from: BLACK RAT\n-Mortality Rate: 75%"));
        return bookTagList;
    }

    public NBTTagList parkinsonsPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.DARK_RED + "Parkinson's:\n\n-Symptoms: 'Butter Fingers,' Jittering\n-Transferred from: OVER-EXPLORING"));
        return bookTagList;
    }

    public NBTTagList feverPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.DARK_RED + "Yellow Fever:\n\n-Symptoms: Nausea, Weakness, Drowsiness, Fatigue\n-Transferred from: MOSQUITOES (Hot Climate)\n-Mortality Rate: 50%"));
        return bookTagList;
    }

    public NBTTagList medsPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.LIGHT_PURPLE + "Medication:\n\n-Made From: TRADING, CRAFTING\n\nItems and Effects:\n-Redstone: Pain Relief\n-Glowstone Dust: Protects Blood Cells\n-Gunpowder: Explosive\n\nWARNING: Use sugar to supress effects!"));
        return bookTagList;
    }

    public NBTTagList bioWarPage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.DARK_BLUE + "Biological Warfare:\n\n" + EnumChatFormatting.DARK_RED + "-WARNING: Illegal in most countries! Beware!" + EnumChatFormatting.DARK_BLUE + "\n\nCraft a chemical extractor and obtain diseases on right click!"));
        return bookTagList;
    }

    public NBTTagList vaccinePage(NBTTagList bookTagList) {
        bookTagList.appendTag(new NBTTagString(EnumChatFormatting.GREEN + "Vaccines:\n" + EnumChatFormatting.DARK_RED + "(Contrary to popular belief does not cause autism)\n\n" + EnumChatFormatting.DARK_GREEN + "-Can be obtained with buying from villagers.\n" + EnumChatFormatting.RED + "-Does not protect from illegal biological warfare!\n" + EnumChatFormatting.DARK_GREEN + "-Mortality Rate: ~99% chance (100% Margin of Error)"));
        return bookTagList;
    }

    @Override
    public void onUpdate(ItemStack itemStack, World world, Entity entity, int unknownInt, boolean unknownBool) {
        super.onUpdate(itemStack, world, entity, unknownInt, unknownBool);
        NBTTagList bookTagList = new NBTTagList();

        if (isFlu && isRabies && isPneumonic && isMalar && isBlackDeath && isParkinsons && isFever && isMeds && isBio && isVaccine){
            bookTagList.appendTag(new NBTTagString("Table of Contents: \n\n" + EnumChatFormatting.DARK_GREEN + "Influenza..................2\nRabies.........................3\n" + EnumChatFormatting.GOLD + "Pneumonic Plague..........4\nMalaria.......................5\n" + EnumChatFormatting.DARK_RED + "Bubonic Plague..........6\nParkinson's................7\nYellow Fever............8\n" + EnumChatFormatting.LIGHT_PURPLE + "Medication.............9\n" + EnumChatFormatting.DARK_BLUE + "Biological Warfare.....10\n" + EnumChatFormatting.GREEN + "Vaccines.........................11"));
        }

        if (isFlu) {
            fluPage(bookTagList);
        }

        if (isRabies) {
            rabiesPage(bookTagList);
        }

        if (isPneumonic) {
            pneumonicPage(bookTagList);
        }

        if (isMalar) {
            malariaPage(bookTagList);
        }

        if (isBlackDeath) {
            blackDeathPage(bookTagList);
        }

        if (isParkinsons) {
            parkinsonsPage(bookTagList);
        }

        if (isFever) {
            feverPage(bookTagList);
        }

        if (isMeds) {
            medsPage(bookTagList);
        }

        if (isBio) {
            bioWarPage(bookTagList);
        }

        if (isVaccine) {
            vaccinePage(bookTagList);
        }

        itemStack.setTagInfo("pages", bookTagList);
        if (entity instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) entity;
            itemStack.setTagInfo("author", new NBTTagString(player.getName()));
        }
        else {
            itemStack.setTagInfo("author", new NBTTagString("Mitchellbrine"));
        }
        itemStack.setTagInfo("title",  new NBTTagString("Medical Journal"));

        NBTTagCompound nbt = itemStack.getTagCompound();
        
        if (entity instanceof EntityPlayer) {
        	
        	if (((EntityPlayer)entity).getCurrentEquippedItem() == itemStack) {
        		EntityPlayer player = (EntityPlayer) entity;
        		
        		ItemStack book = new ItemStack(Items.written_book, 1);
        		
        		book.setTagCompound(nbt);
        		
        		player.setCurrentItemOrArmor(0, book);
        		
        	}
        	
        }

    }

}
