package mc.Mitchellbrine.virusMod.common.item;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;

public class ChemicalBottle extends Item {

    public ChemicalBottle() {
        super();
        //this.setTextureName("VirusMod:chemicalBottle");
        this.setMaxStackSize(1);
        VirusMod.items.add(this);
    }

    /**
     * Called each tick as long the item is on a player inventory. Uses by maps to check if is on a player hand and
     * update it's contents.
     */
    @Override
    public void onUpdate(ItemStack stack, World world, Entity entity, int par4, boolean par5)
    {
        if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
            stack.getTagCompound().setString("chemical", "");
        }
    }
    /**
     * Called when item is crafted/smelted. Used only by maps so far.
     */
    @Override
    public void onCreated(ItemStack stack, World world, EntityPlayer player)
    {
        if (!stack.hasTagCompound()) {
            stack.setTagCompound(new NBTTagCompound());
            stack.getTagCompound().setString("chemical","");
        }
    }

}
