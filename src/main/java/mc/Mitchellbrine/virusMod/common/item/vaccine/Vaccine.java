package mc.Mitchellbrine.virusMod.common.item.vaccine;

import net.minecraftforge.fml.common.registry.GameRegistry;
import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.util.KeyHelper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Vaccine extends Item {

    private String diseaseCured;
    private Potion[] sideEffects;
    private int specialKey;
    private Random random = new Random();

    public Vaccine(String diseaseCured,Potion[] potions, int key) {
        this.diseaseCured = diseaseCured;
        this.sideEffects = potions;
        this.specialKey = key;
        this.setCreativeTab(VirusMod.warfareTab);
        this.setUnlocalizedName("vaccine" + diseaseCured.replaceAll(" ",""));
        //this.setTextureName("VirusMod:syringeMeds");
        this.setMaxStackSize(1);
        this.setMaxDamage(3);
        GameRegistry.registerItem(this,this.getUnlocalizedName().substring(5));
        VirusMod.items.add(this);
    }

    @SuppressWarnings("unchecked")
    public void addInformation(ItemStack itemStack, EntityPlayer player, List lore, boolean par4) {
        lore.add("~~~~~~~~~~");
        lore.add("Cures " + this.diseaseCured);
        lore.add("---");
        if (!KeyHelper.isShiftKeyDown() && !KeyHelper.isCtrlKeyDown()) {
            lore.add(EnumChatFormatting.ITALIC + "*You struggle to read the fine print*");
        } else if ((KeyHelper.isShiftKeyDown() && !KeyHelper.isCtrlKeyDown()) || (!KeyHelper.isShiftKeyDown() && KeyHelper.isCtrlKeyDown())) {
            for (int i = 0; i < sideEffects.length;i++) {
                lore.add(EnumChatFormatting.OBFUSCATED + "SIDE-EFFECT: " + StatCollector.translateToLocal(sideEffects[i].getName()));
            }
        } else if (KeyHelper.isShiftKeyDown() && KeyHelper.isCtrlKeyDown() && !Keyboard.isKeyDown(specialKey)) {
            for (int i = 0; i < sideEffects.length;i++) {
                lore.add("SIDE-EFFECT: " + EnumChatFormatting.OBFUSCATED + StatCollector.translateToLocal(sideEffects[i].getName()));
            }
        } else if (KeyHelper.isShiftKeyDown() && KeyHelper.isCtrlKeyDown() && Keyboard.isKeyDown(specialKey)) {
            for (int i = 0; i < sideEffects.length;i++) {
                lore.add("SIDE-EFFECT: " + StatCollector.translateToLocal(sideEffects[i].getName()));
            }
        } else {
            lore.add(EnumChatFormatting.ITALIC + "*You struggle to read the fine print*");
        }
        lore.add("~~~~~~~~~~");


    }

    @Override
    public int getMaxItemUseDuration(ItemStack itemstack) {
        return 32;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack itemstack) {
        return EnumAction.BOW;
    }

    @Override
    public ItemStack onItemUseFinish(ItemStack itemstack, World world, EntityPlayer player) {
        for(int i = 0; i < sideEffects.length;i++) {
            if (random.nextInt(100) >= 47) {
                player.addPotionEffect(new PotionEffect(sideEffects[i].id,200,0,true,false));
            }
        }
        player.getEntityData().setBoolean("vaccine"+diseaseCured.replaceAll(" ",""),true);
        if (itemstack.getItemDamage() < 2) {
            itemstack.damageItem(1,player);
        } else {
            itemstack = new ItemStack(VirusMod.chemicalExtractor, 1, 0);
        }
        return itemstack;
    }

    @Override
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
        return par1ItemStack;
    }

    }
