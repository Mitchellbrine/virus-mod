package mc.Mitchellbrine.virusMod.common.command;

import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;

import java.util.ArrayList;
import java.util.List;

public class DiseaseCommand implements ICommand
{
    private List aliases;
    public DiseaseCommand()
    {
        this.aliases = new ArrayList();
        this.aliases.add("disease");
        this.aliases.add("sick");
    }

    @Override
    public String getCommandName()
    {
        return "disease";
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender)
    {
        return "disease <diseaseName> <duration> [player]";
    }

    @Override
    public List getCommandAliases()
    {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender icommandsender, String[] astring)
    {
        if (icommandsender instanceof EntityPlayer) {
            EntityPlayer player = (EntityPlayer) icommandsender;

            int duration = 2400;

            if (astring.length > 0) {

                if (astring.length > 1 && !astring[1].isEmpty()) {
                    duration = Integer.valueOf(astring[1]);
                }

                if (astring[0].equalsIgnoreCase("influenza")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.influenza.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("rabies")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.rabies.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("pneumonicPlague")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.pPlague.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("malaria")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.malaria.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("bubonicPlague")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.bubonicPlague.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("parkinsons")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.parkinsons.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("yellowFever")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.yellowFever.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("astigmatism")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.astigmatism.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("heatStroke")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.heatStroke.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("brainReanimation")) {
                    player.addPotionEffect(new PotionEffect(VirusMod.brainReanimate.id, duration, 0));
                } else if (astring[0].equalsIgnoreCase("clear")) {
                    int diseasesRemoved = 0;
                    for (int i = 30; i < 40; i++) {
                        if (DiseaseHandler.removeDisease(player, i)) {
                            diseasesRemoved++;
                        }
                    }
                    player.addChatComponentMessage(new ChatComponentTranslation("diseases.removed",diseasesRemoved));
                } else {
                    icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
                }
            } else {
                icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
            }

        }

    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
    {
        if (icommandsender instanceof EntityPlayer) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring, BlockPos pos)
    {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] astring, int i)
    {
        return false;
    }

    @Override
    public int compareTo(Object o)
    {
        return 0;
    }

}
