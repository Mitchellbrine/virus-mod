package mc.Mitchellbrine.virusMod.common.command;

import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.common.entity.doctor.EntityDoctor;
import mc.Mitchellbrine.virusMod.util.DiseaseHandler;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.*;

import java.util.ArrayList;
import java.util.List;

public class DoctorCommand implements ICommand
{
    private List aliases;
    public DoctorCommand()
    {
        this.aliases = new ArrayList();
        this.aliases.add("killAllDoctors");
        this.aliases.add("killDoc");
    }

    @Override
    public String getCommandName()
    {
        return "killDoctors";
    }

    @Override
    public String getCommandUsage(ICommandSender icommandsender)
    {
        return "/killDocotrs [maxKills/Doctor Name (Use _ for space)] [omit]";
    }

    @Override
    public List getCommandAliases()
    {
        return this.aliases;
    }

    @Override
    public void processCommand(ICommandSender icommandsender, String[] astring)
    {
        if (icommandsender instanceof EntityPlayer) {


            if (astring.length > 0) {
                if (!astring[0].equalsIgnoreCase("help")) {
                    int doctorsKilled = 0;
                    try {
                        int maxKills = Integer.parseInt(astring[0]) - 1;
                        for (int i = 0; i < ((EntityPlayer) icommandsender).worldObj.getLoadedEntityList().size(); i++) {
                            Entity entity = (Entity) ((EntityPlayer) icommandsender).worldObj.getLoadedEntityList().get(i);
                            if (entity instanceof EntityDoctor) {
                                if (doctorsKilled <= maxKills) {
                                    ((EntityDoctor) entity).setHealth(0.0F);
                                    doctorsKilled++;
                                }
                            }
                        }
                    } catch (NumberFormatException ex) {
                        for (int i = 0; i < ((EntityPlayer) icommandsender).worldObj.getLoadedEntityList().size(); i++) {
                            Entity entity = (Entity) ((EntityPlayer) icommandsender).worldObj.getLoadedEntityList().get(i);
                            if (entity instanceof EntityDoctor) {
                                if (astring.length > 1 && astring[1].equalsIgnoreCase("omit")) {
                                    if (!astring[0].replaceAll("_", " ").equalsIgnoreCase(((EntityDoctor) entity).getDoctorName())) {
                                        ((EntityDoctor) entity).setHealth(0.0F);
                                        doctorsKilled++;
                                    }
                                } else {
                                    if (astring[0].replaceAll("_", " ").equalsIgnoreCase(((EntityDoctor) entity).getDoctorName())) {
                                        ((EntityDoctor) entity).setHealth(0.0F);
                                        doctorsKilled++;
                                    }
                                }
                            }
                        }
                    }
                    ((EntityPlayer) icommandsender).addChatComponentMessage(new ChatComponentTranslation("command.doctor.success", doctorsKilled));
                } else {
                    ((EntityPlayer) icommandsender).addChatComponentMessage(new ChatComponentTranslation("command.doctor.usage").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED)));
                }
            } else {
                int doctorsKilled = 0;
                for (int i = 0; i < ((EntityPlayer) icommandsender).worldObj.getLoadedEntityList().size();i++) {
                    Entity entity = (Entity)((EntityPlayer) icommandsender).worldObj.getLoadedEntityList().get(i);
                    if (entity instanceof EntityDoctor) {
                        ((EntityDoctor) entity).setHealth(0.0F);
                        doctorsKilled++;
                    }
                }
                ((EntityPlayer) icommandsender).addChatComponentMessage(new ChatComponentTranslation("command.doctor.success",doctorsKilled));
            }

        }

    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
    {
        if (icommandsender instanceof EntityPlayer) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring, BlockPos pos)
    {
        return null;
    }

    @Override
    public boolean isUsernameIndex(String[] astring, int i)
    {
        return false;
    }

    @Override
    public int compareTo(Object o)
    {
        return 0;
    }

}
