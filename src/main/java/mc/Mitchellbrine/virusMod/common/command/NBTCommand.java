package mc.Mitchellbrine.virusMod.common.command;

import java.util.ArrayList;
import java.util.List;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.BlockPos;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ResourceLocation;

public class NBTCommand implements ICommand
{
  private List aliases;
  public NBTCommand()
  {
    this.aliases = new ArrayList();
    this.aliases.add("diseaseNBT");
    this.aliases.add("dNBT");
  }

  @Override
  public String getCommandName()
  {
    return "diseaseNBT";
  }

  @Override
  public String getCommandUsage(ICommandSender icommandsender)
  {
    return "diseaseNBT <astigmatism/parkinsons> <true/false>";
  }

  @Override
  public List getCommandAliases()
  {
    return this.aliases;
  }

  @Override
  public void processCommand(ICommandSender icommandsender, String[] astring)
  {
    if (icommandsender instanceof EntityPlayer) {
        EntityPlayer player = (EntityPlayer) icommandsender;
        if (astring.length == 0) {
            icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
            return;
        }

        if (astring[0].equalsIgnoreCase("astigmatism")) {
            if (astring[1].equalsIgnoreCase("true")) {
                VirusMod.logger.info("Astigmatism is now true");
                player.getEntityData().setBoolean("gottenAstigmatism",true);
            }
            else if (astring[1].equalsIgnoreCase("false")){
                VirusMod.logger.info("Astigmatism is now false");
                player.getEntityData().setBoolean("gottenAstigmatism",false);
            }
            else {
            icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
                return;
            }
        }
        else if (astring[0].equalsIgnoreCase("parkinsons")) {
            if (astring[1].equalsIgnoreCase("true")) {
                VirusMod.logger.info("Parkinsons is now true");
                player.getEntityData().setBoolean("gottenParkinsons",true);
            }
            else if (astring[1].equalsIgnoreCase("false")) {
                VirusMod.logger.info("Parkinsons is now false");
                player.getEntityData().setBoolean("gottenParkinsons",false);
            }
            else {
            icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
                return;
            }
        }
        else {
        icommandsender.addChatMessage(new ChatComponentText("Invalid arguments"));
            return;
        }
    }

  }

  @Override
  public boolean canCommandSenderUseCommand(ICommandSender icommandsender)
  {
      if (icommandsender instanceof EntityPlayer) {
          return true;
      }
      else {
          return false;
      }
  }

  @Override
  public List addTabCompletionOptions(ICommandSender icommandsender, String[] astring, BlockPos pos)
  {
    return null;
  }

  @Override
  public boolean isUsernameIndex(String[] astring, int i)
  {
    return false;
  }

  @Override
  public int compareTo(Object o)
  {
    return 0;
  }
}