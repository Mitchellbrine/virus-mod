package mc.Mitchellbrine.virusMod.common.potion;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.potion.Potion;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;

import java.io.File;

public class Disease extends Potion{

    private boolean isEnabled;
    private boolean nausea;
    private boolean weakness;
    private boolean slowness;
    private boolean slowDig;
    private int diseaseChance;

	public Disease(int par1, ResourceLocation par2, boolean isBad, int par3, int level, boolean nausea, boolean weakness, boolean slowness, boolean slowDig,String unlocalizedName,int iconX,int iconY,int chance,boolean startEnabled) {
        super(par1, par2, isBad, par3);

        VirusMod.diseases.add(this);

        VirusMod.diseaseLevel.put(this,level);

        Configuration config = new Configuration(new File(VirusMod.configDir.getPath(), "/DiseaseCraft/disease"+par1+".cfg"));

        config.load();

        this.isEnabled = config.get("disease","isEnabled",startEnabled,"Enables/Disables Disease").getBoolean(startEnabled);

        this.nausea = config.get("effects","hasNausea",nausea,"Enables/Disables Nausea").getBoolean(nausea);
        this.weakness = config.get("effects","hasWeakness",weakness,"Enables/Disables Weakness").getBoolean(weakness);
        this.slowness = config.get("effects","hasSlowness",slowness,"Enables/Disables Slowness").getBoolean(slowness);
        this.slowDig = config.get("effects","hasMiningFatigue",slowDig,"Enables/Disables Mining Fatigue").getBoolean(slowDig);
        this.diseaseChance = config.get("disease","diseaseChance",chance,"Changes the chance of disease (usually out of 100)\n\nTrust that the numbers closer to 100 are > and those closer to 0 are < \n\n(Bubonic Plague (30) and Astigmatism (37) are asking which number to be out of)\n\nDefault: " + chance).getInt(chance);

        this.setPotionName(config.get("disease","diseaseName",unlocalizedName,"Sets the unlocalized name for the disease\n\n(Still needs to be localized)").getString());

        this.setIconIndex(config.get("disease","iconX",iconX,"Sets the icon index's x value \n\n(Careful, bad numbers can lead to scary things)").getInt(iconX),config.get("disease","iconY",iconY,"Sets the icon index's y value \n\n(Careful, bad numbers can lead to scary things)").getInt(iconY));

        config.save();

        VirusMod.diseaseConfigs.put(this, config);

		}
	
    /**
     * Sets the index for the icon displayed in the player's inventory when the status is active.
     */
    public Potion setIconIndex(int par1, int par2)
    {
        super.setIconIndex(par1, par2);
        return this;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public boolean hasNausea() {
        return this.nausea;
    }

    public boolean hasWeakness() {
        return this.weakness;
    }

    public boolean hasSlowness() {
        return this.slowness;
    }

    public boolean hasSlowDig() {
        return this.slowDig;
    }

    public int getChance() { return this.diseaseChance; }
}
