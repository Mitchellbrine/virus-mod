package mc.Mitchellbrine.virusMod.common.potion;

import mc.Mitchellbrine.virusMod.VirusMod;
import net.minecraft.client.Minecraft;
import net.minecraft.client.shader.ShaderGroup;
import net.minecraft.client.util.JsonException;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.monster.EntitySilverfish;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.*;
import net.minecraft.world.EnumDifficulty;
import net.minecraft.world.storage.WorldInfo;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.registry.GameRegistry;

import java.util.Random;

public class Effects {

    public static class Jitter {

        public static void execute(Random random, EntityPlayer player) {
            int jitterChance = random.nextInt(750);

            if (jitterChance == 0) {


                int direction = random.nextInt(4);

                if (direction == 0) {
                    if (!player.isPotionActive(VirusMod.parkinsonsMedicine)) {
                        player.moveEntity(1.0F, 0.0F, 0.0F);
                    }
                    else {
                        player.moveEntity(0.25F, 0.0F, 0.0F);
                    }
                } else if (direction == 1) {
                    if (!player.isPotionActive(VirusMod.parkinsonsMedicine)) {
                        player.moveEntity(0.0F, 0.0F, 1.0F);
                    }
                    else {
                        player.moveEntity(0.0F, 0.0F, 0.25F);
                    }
                } else if (direction == 2) {
                    if (!player.isPotionActive(VirusMod.parkinsonsMedicine)) {
                        player.moveEntity(-1.0F, 0.0F, 0.0F);
                    }
                    else {
                        player.moveEntity(-0.25F, 0.0F, 0.0F);
                    }
                } else if (direction == 3) {
                    if (!player.isPotionActive(VirusMod.parkinsonsMedicine)) {
                        player.moveEntity(0.0F, 0.0F, -1.0F);
                    }
                    else {
                        player.moveEntity(0.0F, 0.0F, 0.25F);
                    }
                } else if (direction == 4) {
                    if (!player.isPotionActive(VirusMod.parkinsonsMedicine)) {
                        player.moveEntity(0.0F, 1.0F, 0.0F);
                    }
                    else {
                        player.moveEntity(0.0F, 0.25F, 0.0F);
                    }                }
            }

        }

    }

    public static class Symptoms {

        @Deprecated
        public static void execute(EntityLivingBase entityLiving, boolean nausea, boolean weakness, boolean slowDown, boolean slowDig) {

            if (nausea && VirusMod.stopBitchingAboutNausea == 0) {

               entityLiving.addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 0, true,false));

            }

            if (weakness) {

                entityLiving.addPotionEffect(new PotionEffect(Potion.weakness.id, 100, 0, true,false));

            }

            if (slowDown) {

                entityLiving.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 100, 0, true,false));

            }

            if (slowDig) {

                entityLiving.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 120, 0, true,false));

            }

        }

        public static void execute(EntityLivingBase entityLiving, Potion potion) {

            if (potion instanceof Disease) {
                Disease disease = (Disease) potion;
                if (disease.hasNausea() && VirusMod.stopBitchingAboutNausea == 0) {
                    entityLiving.addPotionEffect(new PotionEffect(Potion.confusion.id, 100, 0, true,false));
                }

                if (disease.hasWeakness()) {
                    entityLiving.addPotionEffect(new PotionEffect(Potion.weakness.id, 100, 0, true,false));
                }

                if (disease.hasSlowness()) {
                    entityLiving.addPotionEffect(new PotionEffect(Potion.moveSlowdown.id, 100, 0, true,false));
                }

                if (disease.hasSlowDig()) {
                    entityLiving.addPotionEffect(new PotionEffect(Potion.digSlowdown.id, 120, 0, true,false));
                }
            } else {
                System.err.println(potion.toString() + " is not an instance of Disease");
            }
        }

    }

    public static class Fumbling {

       public static void execute(EntityPlayer player) {

           if (player.getCurrentEquippedItem() != null) {
                   player.dropOneItem(true);
           }

       }

    }

    public static class Death {

        public static void execute(Random random, EntityLivingBase entityLiving, int deathChance) {

            int deathRand = random.nextInt(100);

            if (deathRand < deathChance) {

            entityLiving.attackEntityFrom(VirusMod.disease, 100.0F);

            }
            else if (deathRand >= deathChance) {

            entityLiving.attackEntityFrom(VirusMod.disease, 10.0F);

            }

        }

    }

    public static class Hydrophobia {

        public static void execute(EntityLivingBase player) {

            if (player.worldObj.getBlockState(new BlockPos(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY), MathHelper.floor_double(player.posZ))) == Blocks.water || player.worldObj.getBlockState(new BlockPos(MathHelper.floor_double(player.posX), MathHelper.floor_double(player.posY), MathHelper.floor_double(player.posZ))) == Blocks.flowing_water) {
                player.attackEntityFrom(DamageSource.drown, 1.0F);
            }

        }

    }

    public static class Seizure {

        public static void execute(Random random, EntityPlayer player) {
            if (random.nextInt(20) < VirusMod.shaderSpeed) {
                if (player.worldObj.isRemote) {

                    int shader = random.nextInt(7);

                    if (shader == 0) {
                        setShader(new ResourceLocation("minecraft:shaders/post/deconverge.json"));
                    } else if (shader == 1) {
                        setShader(new ResourceLocation("minecraft:shaders/post/bumpy.json"));
                    } else if (shader == 2) {
                        setShader(new ResourceLocation("minecraft:shaders/post/flip.json"));
                    } else if (shader == 3) {
                        setShader(new ResourceLocation("minecraft:shaders/post/green.json"));
                    } else if (shader == 4) {
                        setShader(new ResourceLocation("minecraft:shaders/post/outline.json"));
                    } else if (shader == 5) {
                        setShader(new ResourceLocation("minecraft:shaders/post/pencil.json"));
                    } else if (shader == 6) {
                        setShader(new ResourceLocation("minecraft:shaders/post/sobel.json"));
                    } else if (shader == 7) {
                        setShader(new ResourceLocation("minecraft:shaders/post/wobble.json"));
                    }
                }

            }
        }

        public static void setShader(ResourceLocation sha){
            /*Minecraft mc = Minecraft.getMinecraft();
            try{
                mc.entityRenderer = new ShaderGroup(mc.getTextureManager(), mc.getResourceManager(),mc.getFramebuffer(), sha);
            }catch(JsonException e){
                e.printStackTrace();
            }
            mc.entityRenderer.theShaderGroup.createBindFramebuffers(mc.displayWidth, mc.displayHeight); */
            VirusMod.logger.error("Shaders have been disabled due to issues with Forge. We're soory for the inconvenience.");
        }

    }

    public static class FlowStone {

      public static void execute(EntityPlayer p) {

        if (Loader.isModLoaded("flowstonemod")) {

           if (p.inventory.hasItem(GameRegistry.findItem("flowstonemod", "unstableFlowstone"))) {

               if (!p.worldObj.isRemote)
               {
                   WorldInfo worldinfo = MinecraftServer.getServer().worldServers[0].getWorldInfo();
                   Random rand = new Random();
                   int e = rand.nextInt(6);
                   int i = rand.nextInt(45);
                   System.out.println(i + " , " + e);
                   if (i == 45) worldinfo.setRaining(!worldinfo.isRaining());
                   if (i == 43) p.worldObj.spawnEntityInWorld(new EntitySilverfish(p.worldObj));
                   if (i == 42) p.addToPlayerScore(p, 1);
                   if (i == 41) p.addChatMessage(new ChatComponentTranslation("flowstone.removed"));
                   if (i == 40) p.addPotionEffect(new PotionEffect(36, 6000, e));
                   if (i == 39) p.addPotionEffect(new PotionEffect(35, 6000, e));
                   if (i == 38) p.addPotionEffect(new PotionEffect(34, 6000, e));
                   if (i == 37) p.addPotionEffect(new PotionEffect(33, 6000, e));
                   if (i == 36) p.addPotionEffect(new PotionEffect(32, 6000, e));
                   if (i == 35) p.addPotionEffect(new PotionEffect(31, 6000, e));
                   if (i == 34) p.addPotionEffect(new PotionEffect(30, 6000, e));
                   if (i == 33) p.inventory.dropAllItems();
                   if (i == 32) p.attackEntityAsMob(p);
                   if (i == 31) p.setHealth(1F);
                   if (i == 30) p.inventory.dropAllItems();
                   if (i == 29) p.worldObj.setWorldTime(12000);
                   if (i == 28) p.fallDistance = 50 + e;
                   if (i == 27) p.addExhaustion(50F);
                   if (i == 26) p.addExperienceLevel(50);
                   if (i == 00) p.setFire(9999999);
                   if (i == 25) p.worldObj.createExplosion(p, p.posX, p.posY, p.posX, 5, false);
                   if (i == 24) p.worldObj.addWeatherEffect(new EntityLightningBolt(p.worldObj, p.posX, p.posY, p.posZ));
                   if (i == 23) p.worldObj.createExplosion(p, p.posX, p.posY, p.posX, 5, true);
                   if (i == 22) p.setPosition(p.posX += e, p.posY += e, p.posZ += e);
                   if (i == 21) p.addExperienceLevel(4);
                   if (i == 20) p.extinguish();
                   if (i == 19) p.addChatMessage(new ChatComponentTranslation("flowstone.removed"));
                   if (i == 18) p.cameraPitch = 99F;
                   if (i == 15) p.cameraYaw = 0F;
                   if (i == 13) {
                       p.setHealth(1);
                       p.hitByEntity(p);
                       p.attackEntityAsMob(p);
                   }
                   if (i == 12) p.worldObj.setBlockToAir(new BlockPos((int)p.posX, (int)p.posY - 1,(int) p.posZ));
                   if (i == 11) Effects.Seizure.execute(rand, p);
                   if (i == 10) p.addChatMessage(new ChatComponentText("The KKaylium Conspiracy Attacks!"));
                   if (i == 9) p.inventory.addItemStackToInventory(new ItemStack(Items.potato).setStackDisplayName("PotatOS"));

               }
           }

          }

      }

    }

}