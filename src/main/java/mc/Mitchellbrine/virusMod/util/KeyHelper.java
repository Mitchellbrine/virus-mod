package mc.Mitchellbrine.virusMod.util;

import net.minecraft.client.Minecraft;
import org.lwjgl.input.Keyboard;

import java.util.Random;

public class KeyHelper {
        public static boolean isCtrlKeyDown()
        {
            boolean isCtrlKeyDown = Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL);
            if (!isCtrlKeyDown && Minecraft.isRunningOnMac)
                isCtrlKeyDown = Keyboard.isKeyDown(Keyboard.KEY_LMETA) || Keyboard.isKeyDown(Keyboard.KEY_RMETA);

            return isCtrlKeyDown;
        }

        public static boolean isShiftKeyDown()
        {
            return Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT);
        }

        public static int getRandomKey() {
            Random random = new Random();
            int key = 0;
            switch(random.nextInt(6)) {
                case 0: key = Keyboard.KEY_R; break;
                case 1: key = Keyboard.KEY_T; break;
                case 2: key = Keyboard.KEY_Y; break;
                case 3: key = Keyboard.KEY_U; break;
                case 4: key = Keyboard.KEY_I; break;
                case 5: key = Keyboard.KEY_O; break;
                case 6: key = Keyboard.KEY_P; break;
            }
            return key;
        }

}
