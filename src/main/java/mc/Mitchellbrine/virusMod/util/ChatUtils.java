package mc.Mitchellbrine.virusMod.util;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.IChatComponent;
import net.minecraft.world.World;

import java.util.List;

public class ChatUtils {

    @SuppressWarnings("unchecked")
    public static void broadcastMessage(World world, IChatComponent component) {
        List<EntityPlayerMP> players = world.playerEntities;
        for (EntityPlayerMP player : players) {
            player.addChatComponentMessage(component);
        }
    }

}
