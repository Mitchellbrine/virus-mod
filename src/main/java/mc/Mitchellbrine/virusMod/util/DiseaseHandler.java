package mc.Mitchellbrine.virusMod.util;

import mc.Mitchellbrine.virusMod.common.potion.Disease;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.potion.Potion;

public class DiseaseHandler {

    public static boolean isDiseaseActive(EntityLivingBase player, int diseaseID) {
        if (Potion.potionTypes[diseaseID] instanceof Disease) {
            if (player.getEntityData().hasKey("disease" + diseaseID) && ((Disease) Potion.potionTypes[diseaseID]).isEnabled()) {
                return (player.getEntityData().getInteger("disease" + diseaseID) > 0);
            }
        }
        return false;
    }

    public static boolean isDiseaseActive(EntityLivingBase player, Potion potion) {
        if (potion instanceof Disease) {
            if (player.getEntityData().hasKey("disease" + potion.id) && ((Disease) potion).isEnabled()) {
                return (player.getEntityData().getInteger("disease" + potion.id) > 0);
            }
        }
        return false;
    }

    public static boolean isDiseaseActive(Entity player, int diseaseID) {
        if (Potion.potionTypes[diseaseID] instanceof Disease) {
            if (player.getEntityData().hasKey("disease" + diseaseID) && ((Disease) Potion.potionTypes[diseaseID]).isEnabled()) {
                return (player.getEntityData().getInteger("disease" + diseaseID) > 0);
            }
        }
        return false;
    }

    public static boolean isDiseaseActive(Entity player, Potion potion) {
        if (potion instanceof Disease) {
            if (player.getEntityData().hasKey("disease" + potion.id) && ((Disease) potion).isEnabled()) {
                return (player.getEntityData().getInteger("disease" + potion.id) > 0);
            }
        }
        return false;
    }

    public static boolean removeDisease(Entity entity, int diseaseID) {
        if (isDiseaseActive(entity,diseaseID)) {
            entity.getEntityData().setInteger("disease"+diseaseID,0);
            return true;
        }
        return false;
    }


}
