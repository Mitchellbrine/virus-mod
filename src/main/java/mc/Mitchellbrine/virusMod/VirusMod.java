package mc.Mitchellbrine.virusMod;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import mc.Mitchellbrine.virusMod.common.CommonProxy;
import mc.Mitchellbrine.virusMod.common.block.DerpBlock;
import mc.Mitchellbrine.virusMod.common.command.DiseaseCommand;
import mc.Mitchellbrine.virusMod.common.command.DoctorCommand;
import mc.Mitchellbrine.virusMod.common.entity.EntityRat;
import mc.Mitchellbrine.virusMod.common.entity.doctor.EntityDoctor;
import mc.Mitchellbrine.virusMod.common.event.*;
import mc.Mitchellbrine.virusMod.common.item.ChemicalBottle;
import mc.Mitchellbrine.virusMod.common.item.ChemicalExtractor;
import mc.Mitchellbrine.virusMod.common.item.vaccine.Vaccine;
import mc.Mitchellbrine.virusMod.common.potion.Disease;
import mc.Mitchellbrine.virusMod.common.item.Medicine;
import mc.Mitchellbrine.virusMod.common.item.MedicalJournal;
import mc.Mitchellbrine.virusMod.common.potion.ExtraDisease;
import mc.Mitchellbrine.virusMod.common.potion.Medication;
import mc.Mitchellbrine.virusMod.network.PacketHandler;
import mc.Mitchellbrine.virusMod.util.KeyHelper;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.stats.Achievement;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.biome.BiomeGenBase;
import net.minecraftforge.common.AchievementPage;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;

import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.registry.EntityRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = "VirusMod", name = "DiseaseCraft", version = "1.2.0")
public class VirusMod {

	@SidedProxy(clientSide = "mc.Mitchellbrine.virusMod.client.ClientProxy", serverSide = "mc.Mitchellbrine.virusMod.common.CommonProxy")
	public static CommonProxy proxy;

    public static Logger logger = LogManager.getLogger("DiseaseCraft");
    public static Logger entityLogger = LogManager.getLogger("DiseaseCraft-Entity");
    public static Logger debugLogger = LogManager.getLogger("DiseaseCraft-Debug");
    public static Logger integrateLogger = LogManager.getLogger("DiseaseCraft-Integration");

    public static File configDir;

    // Blocks

	public static Block derpBlock;

    //public static Block injecter;
    //public static Block injecterFull;

    private static ArrayList<BiomeGenBase> temperatureBiomes = new ArrayList<BiomeGenBase>();
    public static ArrayList<Item> pPlagueFoods = new ArrayList<Item>();

	// CreativeTabs

    public static CreativeTabs warfareTab = new CreativeTabs("chemicalWarfare") {
        @Override
        public ItemStack getIconItemStack() {
            return new ItemStack(VirusMod.chemicalExtractor,1,30);
        }

        @Override
        public Item getTabIconItem() {
            return VirusMod.chemicalExtractor;
        }
    };

    // Items

    public static ArrayList<Item> items = new ArrayList<Item>();
	
	public static Item tylenol;
	public static Item chloroquine;
    public static Item carbodopa;
    public static Item deadlyMeds;
    public static Item deadlyMeds1;
    public static Item deadlyMeds2;
    public static Item deadlyMeds3;

    public static Item medicalJournal;
    public static Item journalFlu;
    public static Item journalRabies;
    public static Item journalPneumonic;
    public static Item journalMalaria;
    public static Item journalBubonic;
    public static Item journalParkinsons;
    public static Item journalFever;
    public static Item journalMeds;

    public static Item chemicalExtractor;

    public static Item influenzaVaccine;
    public static Item rabiesVaccine;
    public static Item malariaVaccine;
    public static Item pneumonicVaccine;

    public static Item chemicalBottle;
	
	// Potions

	public static Potion bubonicPlague;
	public static Potion rabies;
	public static Potion parkinsons;
	public static Potion influenza;
	public static Potion pPlague;
	public static Potion malaria;
	public static Potion yellowFever;
    public static Potion parkinsonsMedicine;

    public static Potion astigmatism;

    public static Potion heatStroke;

    public static Potion brainReanimate;

    public static Potion shaderPlague;

    public static Potion flowstoneFever;

	public static Potion level1Medicine;
	public static Potion level2Medicine;

	public static DamageSource disease;
	public static DamageSource overdose;

    public static boolean isHalloween;

	// Config Stuff

	public static int addDerpBlock;
	public static int enableDebug;
    public static int addCrazyPotions;
    public static int shaderSpeed;
    public static Property sleepOff;
    public static int sleepLocation;
    public static int stopBitchingAboutNausea;
    public static int useDebugDiseaseMessages;
    public static boolean saveMort;

    // Achievements
	
	//public static Achievement bPlagueAch;
	public static Achievement rabiesAch;

	public static AchievementPage vModAchPage;

    // Api Work

    public static ArrayList<Disease> diseases = new ArrayList<Disease>();
    public static ArrayList<Medication> medications = new ArrayList<Medication>();

    public static ArrayList<Disease> milkProofDiseases = new ArrayList<Disease>();

    public static HashMap<Disease,Integer> diseaseLevel = new HashMap<Disease, Integer>();

    public static HashMap<Disease,Configuration> diseaseConfigs = new HashMap<Disease, Configuration>();

	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		// Okay, so ModJam 4! Let's do some derp!

		proxy.registerStuff();
		registerPotionStuff();

        isHalloween = Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == 31 && Calendar.getInstance().get(Calendar.MONTH) == 10;

		// Configuration
		
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		
		config.load();
		
		addDerpBlock = config.get("blocks", /* Facepalm here! */ "Add Derp Blocks?", 0).getInt();

        addCrazyPotions = config.get("disease", "Add the crazy diseases?", 0).getInt();

        shaderSpeed = config.get("disease", "Shader Speed? (Requires 'addCrazyPotions')", 10).getInt();

		enableDebug = config.get("debug", "Enable Debug?", 0).getInt();

        sleepOff = config.get("disease","Where can you sleep off Influenza?",0,"0 = Singleplayer & Multiplayer | 1 = Singleplayer ONLY | 2 = Multiplayer ONLY");
        sleepLocation = sleepOff.getInt();

        stopBitchingAboutNausea = config.get("disease","Turn off the 1 minute of nausea? (Come on! Are you a wimp?)",0).getInt();

        saveMort = config.get("disease","Save Mort 2014?",true).getBoolean(true);

        useDebugDiseaseMessages = config.get("debug","Turn on disease messages?",0,"Turns on disease messages in console\n\n0 = False\n1 = True").getInt();

		config.save();
		
		// Blocks
		
		if (addDerpBlock == 1) {
		derpBlock = new DerpBlock().setCreativeTab(CreativeTabs.tabMisc).setUnlocalizedName("derpVerticalTest");
		GameRegistry.registerBlock(derpBlock, "derpBlock");
		}

        //injecter = new InjecterEmpty().setCreativeTab(warfareTab).setBlockName("injecter");
        //injecterFull = new Injecter().setBlockName("injecterFull");

        // Villager

        // TODO: Find out how villager trading works!
        // VillagerRegistry.instance().registerVillageTradeHandler(2, new MedicationTrading());

        configDir = event.getModConfigurationDirectory();

        Configuration configHeat = new Configuration(new File(configDir,"/DiseaseCraft/heatOptions.cfg"));

        HeatEvent.setConfigs(configHeat);

		// Items

		
		tylenol = new Medicine(1).setUnlocalizedName("Acetaminophen");
		chloroquine = new Medicine(2).setUnlocalizedName("Chloroquine");
        carbodopa = new Medicine(3).setUnlocalizedName("Carbidopa-Levodopa");
        deadlyMeds = new Medicine(-1).setUnlocalizedName("Acetaminophen ");
        deadlyMeds1 = new Medicine(-1).setUnlocalizedName("Carbidopa-Levodopa ");
        deadlyMeds2 = new Medicine(-1).setUnlocalizedName("Chloroquine ");
        deadlyMeds3 = new Medicine(-2).setUnlocalizedName("Chloroquine  ");

        medicalJournal = new MedicalJournal().setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.full");
        journalFlu = new MedicalJournal(true, false, false, false, false, false, false, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.flu");
        journalRabies = new MedicalJournal(false, true, false, false, false, false, false, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.rabies");
        journalPneumonic = new MedicalJournal(false, false, true, false, false, false, false, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.pneumonic");
        journalMalaria = new MedicalJournal(false, false, false, true, false, false, false, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.malaria");
        journalBubonic = new MedicalJournal(false, false, false, false, true, false, false, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.bubonic");
        journalParkinsons = new MedicalJournal(false, false, false, false, false, true, false, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.parkinsons");
        journalFever = new MedicalJournal(false, false, false, false, false, false, true, false, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.fever");
        journalMeds = new MedicalJournal(false, false, false, false, false, false, false, true, false,false).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("medicalJournal.meds");

        // Diseases
		
		bubonicPlague = new Disease(30, new ResourceLocation("wither"), true, 0, 3,false,true,true,false,"disease.bubonicPlague",1,2,1000,true);
		rabies = new Disease(31, new ResourceLocation("nausea"), true, 0, 1,false,false,true,false,"disease.rabies",6,0,99,true);
		parkinsons = new Disease(32, new ResourceLocation("wither"), true, 0, 3,false,false,true,false,"disease.parkinsons",1,0,1,true);
		influenza = new Disease(33, new ResourceLocation("nausea"), true, 0, 1,true,true,true,true,"disease.influenza",6, 0,95,true);
		pPlague = new Disease(34, new ResourceLocation("poison"), true, 0, 2,false,true,true,true, "disease.pneumonicPlague",3, 1,2,true);
		malaria = new Disease(35, new ResourceLocation("poison"), true, 0, 2,false,true,false,true, "disease.malaria",3, 1,99,true);
		yellowFever = new Disease(36, new ResourceLocation("wither"), true, 0, 3,true,true,true,true,"disease.yellowFever", 1, 2,99,true);

        astigmatism = new Disease(37, new ResourceLocation("wither"), true, 0, 3,false,false,false,false,"disease.astigmatism",1, 0,1000,false);

        heatStroke = new Disease(38, new ResourceLocation("nausea"), true, 0, 1,false,true,true,true,"disease.heat",6,0,95,true);

        brainReanimate = new Disease(39,new ResourceLocation("nausea"), true,0,4,true,true,true,true,"disease.brain",1,2,95,isHalloween);

        if (addCrazyPotions == 1) {
            shaderPlague = new ExtraDisease(50, new ResourceLocation("wither"), true, 0, false).setIconIndex(2, 2).setPotionName("disease.joke.shader");
        }

        if (Loader.isModLoaded("flowstonemod")) {
            integrateLogger.info("Flowstone Mod Detected!");
           flowstoneFever = new ExtraDisease(70, new ResourceLocation("absorption"), true, 0, false).setIconIndex(2, 2).setPotionName("disease.joke.flowstone");

        }

		level1Medicine = new Medication(60, new ResourceLocation("absorption"), false, 16262179).setIconIndex(2, 2).setPotionName("medicine.level1");
		level2Medicine = new Medication(61, new ResourceLocation("absorption"), false, 16262179).setIconIndex(2, 2).setPotionName("medicine.level2");
        parkinsonsMedicine = new Medication(62, new ResourceLocation("absorption"), false, 16262179).setIconIndex(2, 2).setPotionName("medicine.parkinsons");

        // Chemical Extractors

        chemicalExtractor = new ChemicalExtractor().setUnlocalizedName("chemicalExtractor");

        chemicalBottle = new ChemicalBottle().setUnlocalizedName("chemicalBottle");

        int key = KeyHelper.getRandomKey();

        influenzaVaccine = new Vaccine("Influenza",new Potion[]{Potion.confusion,Potion.weakness,Potion.hunger}, key);
        rabiesVaccine = new Vaccine("Rabies",new Potion[]{Potion.confusion,Potion.weakness,Potion.moveSlowdown,Potion.digSlowdown},key);
        malariaVaccine = new Vaccine("Malaria",new Potion[]{Potion.weakness,Potion.blindness,Potion.digSlowdown,Potion.hunger},key);
        pneumonicVaccine = new Vaccine("Pneumonic Plague",new Potion[]{Potion.weakness,Potion.blindness,Potion.digSlowdown,Potion.moveSlowdown,Potion.hunger},key);

        // Damage Sources
		
		disease = new DamageSource("disease").setDamageBypassesArmor().setDamageIsAbsolute();
		overdose = new DamageSource("overdose").setDamageBypassesArmor().setDamageIsAbsolute();

		// Achievements
		
		//bPlagueAch = new Achievement("bPlague", "bPlague", -2, -2, Items.nether_star, (Achievement)null).initIndependentStat().registerStat();
		//rabiesAch = new Achievement("rabies","rabies",0,-2,Items.nether_star, (Achievement)null).initIndependentStat().registerStat();
		
		//vModAchPage = new AchievementPage("vMod", new Achievement[]{bPlagueAch,rabiesAch});
		
		//AchievementPage.registerAchievementPage(vModAchPage);

        //GameRegistry.registerBlock(injecter, "injecter");
        //GameRegistry.registerBlock(injecterFull,"injecterFull");

		GameRegistry.registerItem(tylenol, "tylenol");
		GameRegistry.registerItem(chloroquine, "chloroquine");
        GameRegistry.registerItem(carbodopa, "carbidopa-levidopa");
        GameRegistry.registerItem(deadlyMeds, "deadlymeds");
        GameRegistry.registerItem(deadlyMeds1,"deadlymeds1");
        GameRegistry.registerItem(deadlyMeds2, "deadlymeds2");
        GameRegistry.registerItem(deadlyMeds3, "deadlymeds3");

        GameRegistry.registerItem(medicalJournal, "medicalJournal");
        GameRegistry.registerItem(journalFlu, "medicalJournalFlu");
        GameRegistry.registerItem(journalRabies, "medicalJournalRabies");
        GameRegistry.registerItem(journalPneumonic, "medicalJournalPneumonic");
        GameRegistry.registerItem(journalMalaria, "medicalJournalMalaria");
        GameRegistry.registerItem(journalBubonic, "medicalJournalBubonic");
        GameRegistry.registerItem(journalParkinsons, "medicalJournalParkinsons");
        GameRegistry.registerItem(journalFever, "medicalJournalFever");
        GameRegistry.registerItem(journalMeds, "medicalJournalMeds");

		MinecraftForge.EVENT_BUS.register(new VirusHandler());
		MinecraftForge.EVENT_BUS.register(new OverdosingEvents());
		MinecraftForge.EVENT_BUS.register(new Level2Events());
        MinecraftForge.EVENT_BUS.register(new ChemicalEvents());
        MinecraftForge.EVENT_BUS.register(new HeatEvent());
		
		MinecraftForge.EVENT_BUS.register(new FMLEvents());
		FMLCommonHandler.instance().bus().register(new FMLEvents());

        pPlagueFoods.add(Items.beef);
        pPlagueFoods.add(Items.chicken);
        pPlagueFoods.add(Items.fish);
        pPlagueFoods.add(Items.porkchop);

	}
	
	@Mod.EventHandler
	public void init(FMLInitializationEvent event) {
		EntityRegistry.registerModEntity(EntityRat.class, "rat", 400, this, 80, 3, true);
        EntityRegistry.registerModEntity(EntityDoctor.class, "doctor",401,this,80,3,true);
		
		registerEntityEgg(EntityRat.class, "BlackRat", 7237230, 3158064);
        registerEntityEgg(EntityDoctor.class, "Doctor", 0xFFFFFF, 0x424242);

            PacketHandler.init();
		
			for (int i=0; i < BiomeGenBase.getBiomeGenArray().length;i++) {
                if (BiomeGenBase.getBiome(i) == null || BiomeGenBase.getBiome(i) == BiomeGenBase.hell) {
                    continue;
                }
				if (BiomeGenBase.getBiome(i).temperature >= 0.6F) {
					temperatureBiomes.add(BiomeGenBase.getBiome(i));
					entityLogger.info("Biome Added: " + BiomeGenBase.getBiome(i).biomeName);
				}
			}

            BiomeGenBase[] biomeGenBases = new BiomeGenBase[]{};

			EntityRegistry.addSpawn(EntityRat.class, 5, 1, 4, EnumCreatureType.CREATURE, temperatureBiomes.toArray(biomeGenBases));
            EntityRegistry.addSpawn(EntityDoctor.class,5,1,4,EnumCreatureType.CREATURE, BiomeGenBase.plains,BiomeGenBase.desert);

            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.tylenol, 1), Items.glass_bottle, Items.sugar, Items.redstone, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds, 1), Items.glass_bottle, Items.redstone, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds, 1), Items.glass_bottle, Items.redstone);

            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.carbodopa, 1), Items.glass_bottle, Items.glowstone_dust, Items.sugar, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds1, 1), Items.glass_bottle, Items.glowstone_dust, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds1, 1), Items.glass_bottle, Items.glowstone_dust);
			
		    GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.chloroquine, 1), Items.glass_bottle, Items.gunpowder, Items.redstone, Items.sugar, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds2, 1), Items.glass_bottle, Items.gunpowder, Items.redstone, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds2, 1), Items.glass_bottle, Items.gunpowder, Items.redstone);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.deadlyMeds3, 1), Items.glass_bottle, Items.gunpowder);

            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalFlu, 1), Items.book, Items.water_bucket);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalRabies, 1), Items.book, Items.bone);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalPneumonic, 1), Items.book, Items.porkchop);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalMalaria, 1), Items.book, new ItemStack(Blocks.tallgrass, 1, 1));
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalParkinsons, 1), Items.book, Items.diamond_pickaxe);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalFever, 1), Items.book, Items.fire_charge);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalBubonic, 1), Items.book, Items.rotten_flesh);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.journalMeds, 1), Items.book, Items.redstone, Items.glowstone_dust, Items.gunpowder, Items.sugar);
            GameRegistry.addShapelessRecipe(new ItemStack(VirusMod.chemicalExtractor,1,VirusMod.pPlague.id),new ItemStack(VirusMod.chemicalExtractor,1,0),Items.porkchop);

            GameRegistry.addRecipe(new ItemStack(VirusMod.medicalJournal, 1), "RWb", "FBP", "fGp", 'B', Items.book, 'W', Items.water_bucket, 'b', Items.bone, 'P', Items.porkchop, 'G', new ItemStack(Blocks.tallgrass, 1, 1), 'p', Items.diamond_pickaxe, 'F', Items.fire_charge, 'f', Items.rotten_flesh, 'R', Items.redstone);
            GameRegistry.addRecipe(new ItemStack(VirusMod.chemicalExtractor,1,0),"GLG","G G"," G ",'G',Blocks.glass,'L',Blocks.log);

            proxy.registerItems();

	}

    @Mod.EventHandler
    public void serverStart(FMLServerStartingEvent event) {
        event.registerServerCommand(new DiseaseCommand());
        event.registerServerCommand(new DoctorCommand());
    }
	
	public static int getUniqueEntityId()
	{
		int startEntityId = 300;
		do
		{
			++startEntityId ;
		}
		while (EntityList.getStringFromID(startEntityId) != null);

		return startEntityId;
	}

	@SuppressWarnings("unchecked")
	public static void registerEntityEgg(Class <? extends Entity > entity, String entityName, int primaryColor, int secondaryColor)
	{
		int id = getUniqueEntityId();
        EntityList.addMapping(entity,entityName,id,primaryColor,secondaryColor);
	}


	public static void registerPotionStuff() {
		Potion[] potionTypes = null;
		Field[] potionFields = Potion.class.getDeclaredFields();

		for (Field currentField : potionFields) {
			currentField.setAccessible(true);

			try {
				if (currentField.getName().equals("potionTypes") || currentField.getName().equals("field_76425_a")) {

                    if (Potion.potionTypes.length < 256) {
                        Field error = Field.class.getDeclaredField("modifiers");
                        error.setAccessible(true);
                        error.setInt(currentField, currentField.getModifiers()
                                & -17);
                        potionTypes = (Potion[]) ((Potion[]) currentField
                                .get((Object) null));
                        Potion[] newPotionTypes = new Potion[256];
                        System.arraycopy(potionTypes, 0, newPotionTypes, 0,
                                potionTypes.length);
                        currentField.set((Object) null, newPotionTypes);
                    }
					
				}
			} catch (Exception exception) {
				System.err
						.println("Severe error, please report this to the mod author! Crash Log:");
				exception.printStackTrace();
			}
		}
	}

}