package mc.Mitchellbrine.virusMod.client;

import com.google.common.collect.Lists;
import mc.Mitchellbrine.virusMod.common.item.ChemicalExtractor;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemModelMesher;
import net.minecraft.client.renderer.entity.RenderItem;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.potion.Potion;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import mc.Mitchellbrine.virusMod.VirusMod;
import mc.Mitchellbrine.virusMod.client.render.*;
import mc.Mitchellbrine.virusMod.common.CommonProxy;
import mc.Mitchellbrine.virusMod.common.entity.EntityRat;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import mc.Mitchellbrine.virusMod.common.entity.doctor.EntityDoctor;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.item.Item;
import net.minecraftforge.common.MinecraftForge;

@SideOnly(Side.CLIENT)
public class ClientProxy extends CommonProxy{

	@Override
	public void registerStuff() {
		RenderingRegistry.registerEntityRenderingHandler(EntityRat.class, new RenderRat(Minecraft.getMinecraft().getRenderManager()));
        RenderingRegistry.registerEntityRenderingHandler(EntityDoctor.class, new RenderDoctor(Minecraft.getMinecraft().getRenderManager(),new ModelBiped(), 0.3F));

        MinecraftForge.EVENT_BUS.register(new TemperatureGUI());

        //MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(VirusMod.injecter), new InjecterHandler());
        //MinecraftForgeClient.registerItemRenderer(Item.getItemFromBlock(VirusMod.injecterFull), new InjecterFullHandler());
	}
        
    @Override
    public void registerItems() {

        RenderItem renderItem = FMLClientHandler.instance().getClient().getRenderItem();
        ItemModelMesher mesher = renderItem.getItemModelMesher();

        mesher.register(VirusMod.chemicalExtractor,0,new ModelResourceLocation("VirusMod:chemicalExtractor","inventory"));
        ModelBakery.addVariantName(VirusMod.chemicalExtractor, "VirusMod:chemicalExtractor");

        for (int i = 30; i < 40; i++) {
                if (Potion.potionTypes[i] != null) {
                    ModelBakery.addVariantName(VirusMod.chemicalExtractor, "VirusMod:chemicalExtractor"+i);
                    mesher.register(VirusMod.chemicalExtractor, i, new ModelResourceLocation("VirusMod:chemicalExtractor"+i, "inventory"));
                }
            }
            for (Item item : VirusMod.items) {
                if (item instanceof ChemicalExtractor) continue;
                ModelBakery.addVariantName(item,"VirusMod:"+item.getUnlocalizedName().substring(5).replaceAll(" ","0").toLowerCase());
                mesher.register(item, 0, new ModelResourceLocation("VirusMod:" + item.getUnlocalizedName().substring(5).replaceAll(" ","0").toLowerCase(), "inventory"));
            }


    }
	
}
