package mc.Mitchellbrine.virusMod.client.render;

import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import mc.Mitchellbrine.virusMod.common.event.HeatEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.client.event.RenderGameOverlayEvent;

/**
 * Created by Mitchellbrine on 2014.
 */
public class TemperatureGUI extends GuiScreen {

    @SubscribeEvent(priority = EventPriority.NORMAL)
    public void onRenderExperienceBar(RenderGameOverlayEvent event) {
        // Render only after everything else has renderered
        if (event.isCancelable() || event.type != RenderGameOverlayEvent.ElementType.ALL) {
            return;
        }

        // Render only if the player is holding a wand
        if (Minecraft.getMinecraft().thePlayer == null) {
            return;
        }


        if (HeatEvent.blockModifierEnabled || HeatEvent.biomeModifierEnabled) {
            if (Minecraft.getMinecraft().thePlayer.getEntityData().hasKey("heatDC")) {
                Minecraft.getMinecraft().fontRendererObj.drawString("Current Temperature: " + Minecraft.getMinecraft().thePlayer.getEntityData().getFloat("heatDC"), 1, 1, 0xFF0000);
            }
        }

    }

}
