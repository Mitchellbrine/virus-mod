package mc.Mitchellbrine.virusMod.client.render;

import mc.Mitchellbrine.virusMod.common.entity.doctor.EntityDoctor;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.MathHelper;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderDoctor extends RenderLiving {

    protected ModelBiped model;
    private static final ResourceLocation doctorTex0 = new ResourceLocation("VirusMod:textures/entity/doctorSkin.png");
    private static final ResourceLocation doctorTex1 = new ResourceLocation("VirusMod:textures/entity/doctorSkin2.png");
    private static final ResourceLocation doctorTex2 = new ResourceLocation("VirusMod:textures/entity/doctorSkin3.png");
    private static final ResourceLocation doctorTex3 = new ResourceLocation("VirusMod:textures/entity/doctorSkin4.png");

    public RenderDoctor(RenderManager manager, ModelBiped modelBiped, float f)
    {
        super(manager, modelBiped, f);
        this.model = (ModelBiped)this.mainModel;
    }

    public void renderDoctor(EntityDoctor entity, double x, double y, double z, float yaw, float partialRenderTicks)
    {
        super.doRender(entity, x, y, z, yaw, partialRenderTicks);
    }

    public void doRenderLiving(EntityDoctor living, double x, double y, double z, float yaw, float partialRenderTicks)
    {
        this.renderDoctor((EntityDoctor)living, x, y, z, yaw, partialRenderTicks);
    }

    /**
     * Actually renders the given argument. This is a synthetic bridge method, always casting down its argument and then
     * handing it off to a worker function which does the actual work. In all probabilty, the class Render is generic
     * (Render<T extends Entity) and this method has signature public void doRender(T entity, double d, double d1,
     * double d2, float f, float f1). But JAD is pre 1.5 so doesn't do that.
     */
    public void doRender(Entity entity, double x, double y, double z, float yaw, float partialRenderTicks)
    {
        this.renderDoctor((EntityDoctor)entity, x, y, z, yaw, partialRenderTicks);
    }

    /**
     * Returns the location of an entity's texture. Doesn't seem to be called unless you call Render.bindEntityTexture.
     */
    @Override
    protected ResourceLocation getEntityTexture(Entity entity)
    {
        ResourceLocation mobTexture = null;
        if (entity instanceof EntityDoctor) {
            if (((EntityDoctor) entity).getDoctorName().equals("Dr. Sore N. Chests")) {
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorSoaryn.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Doctor Octagonapus")) {
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorOcto.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Dr. Dinnerbone")) {
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorDinnerbone.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Dr. entity.dr.name")) {
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorNull.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Dr. Isom")){
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorKayla.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Dr. Mills")){
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorPoppy.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Dr. Smith")){
                mobTexture = new ResourceLocation("VirusMod:textures/entity/doctorComp.png");
            } else if (((EntityDoctor) entity).getDoctorName().equals("Dr. Huntley")){
                mobTexture = doctorTex2;
            } else{
                switch (((EntityDoctor) entity).getProfession()) {
                    case 0:
                        mobTexture = doctorTex0;
                        break;
                    case 1:
                        mobTexture = doctorTex1;
                        break;
                    case 2:
                        mobTexture = doctorTex2;
                        break;
                    case 3:
                        mobTexture = doctorTex3;
                        break;
                }
            }
        }
        return mobTexture;
    }

    @Override
    protected void rotateCorpse(EntityLivingBase entity, float x, float y, float z)
    {
        GL11.glRotatef(180.0F - y, 0.0F, 1.0F, 0.0F);

        if (entity.deathTime > 0)
        {
            float f3 = ((float)entity.deathTime + z - 1.0F) / 20.0F * 1.6F;
            f3 = MathHelper.sqrt_float(f3);

            if (f3 > 1.0F)
            {
                f3 = 1.0F;
            }

            GL11.glRotatef(f3 * this.getDeathMaxRotation(entity), 0.0F, 0.0F, 1.0F);
        }
        else
        {
            if (entity instanceof EntityDoctor) {
                String s = ((EntityDoctor) entity).getDoctorName();

                if (s.equals("Dr. Dinnerbone")) {
                    GL11.glTranslatef(0.0F, entity.height + 0.1F, 0.0F);
                    GL11.glRotatef(180.0F, 0.0F, 0.0F, 1.0F);
                }
            }
        }
    }

}
